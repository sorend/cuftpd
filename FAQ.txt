Q: I get the error "Failed to connect to userbase. (Found registry but not userbase)" when I try to run cuftpd with a remote statistics.
A: See below.

Q: I get the error "Failed to connect to userbase. (Found registry but not userbase)" when I try to run cuftpd with a remote userbase.
A: This is because you have some connectivity error. It finds the registry (which is what you specify on cuftpd.xml),
   but it doesn't find the actual userbase. This is probably due to connectivity errors, such as firewalls etc.
   Make sure the appropriate ports are open (you can set which ports to use in userbase.conf).

Q: Where can I find the SVN repository for cuftpd?
A: at https://cuftpd.svn.sourceforge.net/svnroot/cuftpd (if you want to check stuff out)
   or at http://cuftpd.svn.sourceforge.net/viewvc/cuftpd/ (if you want to look around)

Q: My symlinks show up as files in cuftpd, why?
A: That's because they are broken, and don't point to anything.

Q: But they work in glftpd!
A: Well that's because glftpd works in a chrooted environment, and cuftpd doesn't (at least not by default)
Thus, make sure your symlinks are resolvable, and then they will work.

Q: I have a problem with my remote userbase. When I connect to the ftpd, i get the following message:
Connection refused to host: <IP HERE, OR POSSIBLY NOTHING>; nested exception is:
        java.net.ConnectException: Connection refused
reinitializing in remote userbase
Trying to re-connect to remote userbase, tries: 1 sleeping for 5 seconds

A1: The problem is that the RMI server on the remote userbase doesn't know how to export the objects, so it tries to export
it the only way it can, on localhost.
Running the userbase with this command line fixes the problem:
# java -Djava.rmi.server.hostname=<IP OF REMOTE USERBASE> -jar userbase.jar
NOTE: This is fixing the symptom, not the problem.
A2: This, however, is the real fix. Something that should have been done from the beginning.
Make sure that the computer running the remote userbase can resolve its own ip!
This is most easily done by adding a like this in /etc/hosts on the box running the remote userbase:
<IP OF REMOTE USERBASE> myRemoteUserbaseHostname
Note that this cannot be 127.0.0.1, but rather it has to be the acting external ip of the computer.

Q: The output from my "site"-commands as well as the statline look funny, what's going on?
A: That's because the default templates in cuftpd use 8bit ascii characters. These 8-bit characters are not compatible
   with all fonts. Any terminal-based monospace font will work. I recommend "terminal", 6 points, in windows.
   If you don't want this, however, there are some 7bit templates in the cuftpd/data/templates directory.
   Their names end with "_7bit.txt", and to use them you simply remove the "_7bit" part from the name.
   This will make them the default display templates for cuftpd.
   NOTE: you should make backups of the 8bit templates first, in case you need them later.
   This applies to the following files:
   data/templates/lines_7bit.txt
   data/templates/lines.txt
   data/templates/user_template_7bit.txt
   data/templates/user_template.txt
   data/templates/group_template_7bit.txt
   data/templates/group_template.txt
   data/templates/race_template_7bit.txt
   data/templates/race_template.txt


Q:  Why does cuftpd only use 168bit crypto when I see glftpd using 256bit and different cipher suites?
    (a.k.a I want stronger crypto for SSL/TLS, give it to me)
A:  You need the UnlimitedStrength crypto policy files from sun. Since there are export issues, I can't include them in
    the release, but you can get them from here: http://java.sun.com/javase/downloads/index.jsp (close to the bottom,
    in the "Other Downloads" section.
    (Be sure to put them in the correct jre/lib/security dir (I just put them in all my lib/security dirs), or they won't
    be found)

    NOTE: I have also experienced a 3 fold increase in transfer speed when using the unlimited strength policy files.

Q: Help, my remote userbase spews out a bunch of garbage (it's called a stack trace, folks) when I try to connect to it!
A: If it looks something like this in the beginning:
2007-nov-28 21:17:04 sun.rmi.transport.tcp.TCPTransport$AcceptLoop executeAcceptLoop
WARNING: RMI TCP Accept-1099: accept loop for ServerSocket[addr=0.0.0.0/0.0.0.0,port=0,localport=1099] throws

Then there is something wrong with the underlying socket.
A further inspection of what it says may reveal the error.
Unfortunately I can't stop all that from being dumped on the console, but the most common error is that the password for
the keystore and/or truststore that the remote userbase uses was wrong.
Change this in data/userbase.conf

Q: I run cuftpd without "bind_address" and "pasv_address", but when I connect to "localhost", I am unable to FXP, why?
A: This is because of which IP the passive socket binds on. As a rule, never connect to localhost unless you only want
   to do local stuff.
   There are combinations of values for "bind_address" and "pasv_address" that may seem like they would work, but they
   don't. Just connect to the "real" ip of the computer running cuftpd to do FXP transfers.
   
Q: I'm getting an error when I start cuftpd with remote userbase here:
java.security.UnrecoverableKeyException: Cannot recover key at sun.security.provider.KeyProtector.recover(KeyProtector.java:311)
<snip>
at cu.ftpd.ssl.DefaultSSLSocketFactory.<init>(DefaultSSLSocketFactory.java:71)
at cu.ftpd.ssl.DefaultSSLSocketFactory.initialize(DefaultSSLSocketFactory.java:98)
at cu.ftpd.Server.<init>(Server.java:112)
at cu.ftpd.Server.main(Server.java:285)
But the password is correct and works with keytool -list -keystore <store>
Keystore type: JKS
Keystore provider: SUN
Your keystore contains 2 entries
master, Dec 11, 2007, trustedCertEntry,
..
mykey, Dec 11, 2007, PrivateKeyEntry,
..
I used master instead of server
If I change the password to something invalid, I get this error:
Failed to start server: Keystore was tampered with, or password was incorrect

A: Ah, figured it out.
I created the keystores, but then I changed the password on them. What I didn't know was that this changes the password
on the keystore, but _NOT_ the private key.
And the SSL context requires the private key and the keystore to have the same password
So if those two differ, you get that error. Perhaps something to put in the README or a FAQ or TROUBLESHOOTING file.
Fixed it by doing: keytool -keypasswd -keystore client.keystore -alias mykey 