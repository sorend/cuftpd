<?xml version="1.0"?>
<ftpd xmlns="https://bitbucket.org/jevring/cuftpd/1.0.0">
    <main>
        <port>1600</port>
        <max_clients>60</max_clients>
        <greeting>Welcome!</greeting>
        <show_version>true</show_version>
        <!-- true here if you wish the cuftpd version to be
        displayed upon connect (in the 220 message), false otherwise -->
        <idle_timeout>60</idle_timeout>
        <connection_timeout>10</connection_timeout>
        <data_directory>data/</data_directory>
        <max_uploaders>30</max_uploaders>
        <max_downloaders>30</max_downloaders>
        <!-- OPTIONAL. Use this if the server is behind a NAT.
        This is the ip that will be DISPLAYED. The socket will
        actually still bind to a local ip. -->
        <pasv_address></pasv_address>
        <!-- OPTIONAL. Example: 32000-65000 Note that if you set
        this to a very small range, you WILL run out of sockets,
        even for one connection. -->
        <pasv_port_range></pasv_port_range>
		<!-- OPTIONAL. Leaving bind_address empty means that it will bind to some OS-specific
			default address. This MAY be all interfaces (on windows), or it may be localhost (linux)
			or anything else. If you wish to bind to all interfaces, try using 0.0.0.0 as an address.
			If you want to specify an interface, do that here with the ip/dns.
			NOTE: "localhost" binds ONLY on localhost, making it impossible to
			reach from any other computers!  -->
        <bind_address></bind_address>
        <!-- OPTIONAL. Put the addresses of any entry
        bouncers to be used here separated by spaces -->
        <bouncers></bouncers>
        <!-- OPTIONAL. If set to 'true', only connections from any of the
        listed bouncers will be accepted. Even if this is set to 'true',
        connections from localhost will always be accepted. -->
        <bouncer_only>false</bouncer_only>
        <!-- If set to 'true', cuftpd will transfer files in ASCII mode without translating
        line breaks. This is approximately twice as fast (currently) than translating line
        breaks. Setting this to true *seems* to violate RFC959, but it's not all that clear. -->
        <fast_ascii_transfer>false</fast_ascii_transfer>

    </main>
    <epsv>
        <!-- Enables the EPSV response will be non-standard and include the IP address and version
        of the connection as follows: 229 Entering Extended Passive Mode (|$version|$ip|$port|)
        Possible version are allow, deny and force.
        If force, the EPSV response will always be as described above.
        If deny, it will always be standard.
        If allow, the mode can be switched by the client by issuing "EPSV EXTENDED/NORMAL" as needed.
        To check which mode the ftpd is in, issue a "EPSV MODE" command. This will response with
        first tye configuration type (allow, deny or force) and then with the current mode (NORMAL, EXTENDED)
        inside parentheses. For example: "200 allow (EXTENDED)"

        When in mode "allow", it will default to the rfc response,
        and only go to the extended if instructed to by the client.
        -->
        <extended_epsv_response>allow</extended_epsv_response>
        <!-- Use this if you want to FORCE the ipv4 address to bind to when handling EPSV 1.
        If you leave this empty, an address will be chosen from the ones available on the machine. -->
        <ipv4_bind_address></ipv4_bind_address>
        <!-- Use this if you want to FORCE the ipv6 address to bind to when handling EPSV 2.
        If you leave this empty, an address will be chosen from the ones available on the machine. -->
        <ipv6_bind_address></ipv6_bind_address>
        <!-- "1", "2", or "1,2"
         It is important that you only specify "1" if you don't actually have an ipv6 interface.
         This can be used to force either ipv4 (1) only or ipv6 (2) only.
         However, leaving "1,2" in place when only one is supported can be confusing to clients.
         Regardless of this setting, the actual availability of an interface of the specified
         type will be checked when a connection is required.
         -->
        <valid_protocol_families>1,2</valid_protocol_families>
    </epsv>
    <ssl>
        <mode>1</mode><!-- 1=optional, 2=force, 3=ftps (a.k.a implicit ssl) -->
        <data_connection_mode>1</data_connection_mode><!-- 1=optional, 2=force -->
        <keystore>
            <location>data/client.keystore</location>
            <password>client</password>
        </keystore>
        <truststore>
            <location>data/client.keystore</location>
            <password>client</password>
        </truststore>
    </ssl>
    <filesystem>
        <default><!-- NOTE: without these default filesystem settings, cuftpd WILL NOT START! -->
            <path>/cuftpd/site</path><!-- real path -->
            <group>cuftpd</group><!-- the owner displayed for files and directories with no metadata -->
            <owner>cuftpd</owner><!-- the group displayed for files and directories with no metadata -->
            <ratio>3</ratio>
        </default>
        <sections>
            <!-- Sections are OPTIONAL. If you don't want to use them, just remove them.
            They are just here to show you how they work. Each section inherits all the
            information from the default section unless explicitly overridden.
            (i.e. if you want all your sections to have the same ratio as the default,
            just remove the <ratio> part from the sections).
            All section paths are relative to the root of the ftpd
             -->
            <!-- Here are some sample sections
            <section>
                <path>/mp3</path>
                <name>mp3</name>
                <group>mp3_grp</group>
                <owner>mp3</owner>
                <ratio>3</ratio>
            </section>
            <section>
                <path>/dvdr</path>
                <name>dvdr</name>
                <owner>dvdr</owner>
            </section>
            -->
        </sections>
        <!-- since these might be common between several installations,
        we don't force them to use the data dir -->
        <welcome_msg>data/welcome.txt</welcome_msg>
        <goodbye_msg>data/goodbye.txt</goodbye_msg>
        <rules>data/rules.txt</rules>
        <forbidden_files>permissions.acl groups</forbidden_files><!-- Note: does not accept wildcards -->
        <!-- files that are not included in listings and forbidden to download or upload -->
        <free_files>*.nfo</free_files>
        <!-- files that can be downloaded without loss of credits-->
    </filesystem>
    <logging>
        <!-- NOTE: to disable a log either remove the filename or specify "none" as the name.-->
        <eventlog>data/logs/cuftpd.log</eventlog>
        <errorlog>data/logs/error.log</errorlog>
        <securitylog>data/logs/security.log</securitylog>
        <command_log>
            <!--
             This command log enables the administrator to log every command received and every response sent on the server.
             This can be useful for debugging or auditing.
             This logging can be turned on or off at runtime by invoking the "site commandlog" command.
             This command requires that the per has the UserPermission.SHUTDOWN permission.
            -->
            <!--
            verbosity can have any of the following values:
            0. Off [USE THIS SETTING TO TURN THIS LOG OFF!]  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            1. Log commands but not responses
            2. Log both commands AND responses
            -->
            <verbosity>2</verbosity>
            <!--
            file_mode can have any of the following values:
            1. One file for everything
            2. One file per username
            3. One file per username and connectionId
            -->
            <file_mode>1</file_mode>
            <directory>data/logs/commandlog/</directory><!-- The directory in which the log(s) should be created -->
        </command_log>
    </logging>
    <user>
        <authentication>
            <!-- types: 1=cuftpd, 2=remote(rmi), 3=anonymous, 4=asynchronous
            6=remote(sql)(not implemented) 7=remote(ldap/ad)(not implemented) -->
            <type>4</type>
            <remote>
                <host>192.168.0.125</host>
                <port>1099</port><!-- 1099 is default for RMI -->
                <retry_interval>5</retry_interval>
                <!-- seconds after a disconnect is discovered
                that we try to reconnect to the userbase -->
            </remote>
            <asynchronous>
                <!--
                In some cases (don't ask me which, I've never had this issue), you need to ensure that your
                keystore contains an entry where the alias matches the <name> tag below. This value is case
                sensitive!
                
                NOTE: Don't use 'localhost' anywhere in the URI unless you intend to run the network ONLY
                on localhost. Specifying 'localhost' means that the messages cannot "escape" the machine
                itself. Please only use real ip-addresses or host-names.
                -->
                <name>one</name>
                <uri>tcp://192.168.0.102:1800</uri>
                <peers>
                    <peer>
                        <name>two</name>
                        <uri>failover:(tcp://192.168.0.105:1700)</uri>
                    </peer>
					<!-- 
					<peer>
						<name>three</name>
						<uri>failover:(tcp://192.168.0.115:1700)</uri>
					</peer>
					-->
                </peers>
            </asynchronous>
        </authentication>
        <statistics>
            <!-- types: 0=off, 1=cuftpd, 2=remote(rmi) -->
            <type>1</type>
            <remote>
                <host>192.168.1.100</host>
                <port>1099</port><!-- 1099 is default for RMI -->
                <retry_interval>5</retry_interval>
            </remote>
        </statistics>
    </user>
    <events>
        <!--
        Type can be either "java" or "shell"

        NOTE: events triggered "after" do not occur if the command isn't completed.
        A command is not completed if there was some error in processing, or if a "before"
        event handler terminated execution.
         The events that are currently available are:
         CreateDirectory
         RemoveDirectory
         DeleteFile
         RenameFrom
         RenameTo
         SiteCommand
         Upload
         Download
        -->
        <handler><!-- These two event handlers are responsible for writing the NEWDIR and DELDIR events to cuftpd.log -->
            <time>after</time>
            <event>CreateDirectory</event>
            <type>java</type>
            <path>cu.ftpd.filesystem.eventhandler.FileSystemEventHandler</path>
        </handler>
        <handler>
            <time>after</time>
            <event>RemoveDirectory</event>
            <type>java</type>
            <path>cu.ftpd.filesystem.eventhandler.FileSystemEventHandler</path>
        </handler>
    </events>
    <commands>
        <!-- These commands are executable by doing "site $command" where $command is the name of the command -->
        <command>
            <type>shell</type><!-- can be either "shell" or "java". Defaults to "shell" if omitted. -->
            <name>pre</name>
            <path>/path/to/prescript</path>
            <add_response_code>true</add_response_code>
            <!--
            Set to add_response_code=true if cuftpd should add a "200- " and "200 " respectively, at the beginning of each line.
            If the command returns anything other than 0 and add_response_code is set to 'true', the response code will be 500
            Set to add_response_code=false if the command must echo a response code by itself.
            IMPORTANT: if you set type=java, then add_response_code will not work. Then the command has to do it on its own
            To add help for a command, put a file named command.txt in data/help/, where "command" is the name of the command
            -->
        </command>
        <command>
            <type>java</type>
            <name>invite</name>
            <path>cu.ftpd.commands.site.actions.Invite</path>
        </command>
    </commands>
    <modules>
        <module>
            <active>true</active>
            <name>dupecheck</name>
            <class>cu.ftpd.modules.dupecheck.DupeCheckModule</class>
            <settings>
                <dupecheck>
                    <log>data/logs/dupes.log</log>
                    <days>7</days>
                    <case_sensitive>false</case_sensitive>
                    <files>*.r?? *.mp3 *.zip</files>
                </dupecheck>
            </settings>
        </module>
        <module>
            <active>true</active>
            <name>requests</name>
            <class>cu.ftpd.modules.requests.RequestsModule</class>
            <settings>
                <requests>
                    <log>data/logs/requests.log</log>
                    <auto_approve>true</auto_approve>
                </requests>
            </settings>
        </module>
        <module>
            <active>true</active>
            <name>dirlog</name>
            <class>cu.ftpd.modules.dirlog.DirlogModule</class>
            <settings>
                <dirlog>
                    <log>data/logs/directories.log</log>
                    <enable_dirscript>false</enable_dirscript>
                    <!--
                    Setting this to 'true' enables a module that will only allow
                    a directory to be created on the site of the directory does not already exist.
                    The entry bellow indicates which patterns (regular expression) should be skipped when checking.
                    -->
                    <dirscript_exclusions>cd\d|disc\d|dvd\d</dirscript_exclusions>
                </dirlog>
            </settings>
        </module>
        <module>
            <active>true</active>
            <name>xferlog</name>
            <class>cu.ftpd.modules.xferlog.XferlogModule</class>
            <settings>
                <xferlog>
                    <log>data/logs/xferlog</log>
                </xferlog>
            </settings>
        </module>
        <module>
            <active>true</active>
            <name>nukehandler</name>
            <class>cu.ftpd.modules.nukehandler.NukeHandlerModule</class>
            <settings>
                <nukehandler>
                    <log>data/logs/nukes.log</log>
                </nukehandler>
            </settings>
        </module>
        <module>
            <active>true</active>
            <name>site_command_logger</name>
            <class>cu.ftpd.modules.sitecommandlogger.SiteCommandLoggerModule</class>
            <settings>
                <site_command_logger>
                    <log>data/logs/site_commands.log</log>
                    <commands>addgadmin, delgadmin, addip, addgroup, adduser, autg, give, take, delgroup, delip, deluser, gadduser, groupchange, addpermissions, delpermissions, passwd, primarygroup, rufg, tagline, change</commands>
                    <!-- the commands that should be logged. '*' indicates that ALL site commands should be logged-->
                </site_command_logger>
            </settings>
        </module>
        <module>
            <active>true</active>
            <name>zipscript</name>
            <class>cu.ftpd.modules.zipscript.ZipscriptModule</class>
            <settings>
                <zipscript>
                    <!-- 1=cuftpd 2=pzs-ng 3=shell, 4=custom (java), 0=none/disabled -->
                    <type>1</type>
                    <on_the_fly_crc>true</on_the_fly_crc>
                    <!-- set to 'true' if you want the CRC to be
                    calculated as the file is being uploaded. -->
                    <cuftpd><!-- type=1 -->
                        <files>*.r?? *.s?? *.mp3 *.zip *.sfv</files><!-- *.s?? is for rar-files after .r99 -->
                        <!-- Only run checks for these filename patterns. -->
                        <template>data/templates/race_template.txt</template>
                        <short_name>CU</short_name>
                    </cuftpd>
                    <pzs_ng><!-- type=2 -->
                        <path>/cuftpd/zipscript/bin/</path>
                    </pzs_ng>
                    <shell><!-- type=3 -->
                        <!--
                        These entries should contain the paths to the binaries/scripts that you want
                        to execute AFTER these events have taken place. A zipscript is somewhat different
                        an event handler, even though most of the semantics are the same.
                        Since this was borne out of a generalization of pzg-ng, it still retains the same
                        parameter list
                        -->
                        <upload>/cuftpd/bin/zipscript/zipscript</upload><!-- parameters: <absolute filepath> <crc> <user> <group> <tagline> <speed> <section> -->
                        <delete>/cuftpd/bin/postdel.sh</delete><!-- parameters: <user> <group> <tagline> <section> <DELE command> -->
                        <rescan>/cuftpd/bin/rescan</rescan><!-- parameters: <user> <group> <tagline> <section> <cwd> <rescan arguments> -->
                    </shell>
                    <custom><!-- type=4 -->
                        <!--
                        The class name of the zipscript. This class must implement the cu.ftpd.modules.zipscript.Zipscript
                        interface. This class will be instantiated only once, so you must take care to make all the methods
                        re-entrant.
                        -->
                        <classname>cu.ftpd.modules.zipscript.internal.CuftpdZipscript</classname>
                    </custom>
                </zipscript>
            </settings>
        </module>
    </modules>
</ftpd>
