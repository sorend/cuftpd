Syntax: site search [-raw] pattern

Lists all directories that match the provided pattern, 
and information about size and location of those directories
[-raw] displays the results in a machine readable form
This means that search terms that begin with "-raw", such
as "-raw samples" would have to be altered to "*-raw samples"