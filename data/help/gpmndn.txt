Syntax: site gpmndn [-number=n] [-section=name] [-group=g] [-raw=b]

Lists the download statistics based on groups for this month
Requires: UserPermission.STATISTICS
See also: gpalldn, gpallup, gpdaydn, gpdayup, gpmndn, gpmnup,
gpwkdn, gpwkup
Options:
-number=n       where 'n' is the number of entries to see

-section=name   where 'name' is the name of the section
                for which we want to display information

-raw=b          where 'b' is 'true' or 'false'. If set to 'true',
                this option presents the data in a format
                that is easier for machines to parse

-group=g        where 'g' is the name of the group we are
                interested in. Omitting this parameter lists
                the inter-group rankings. Including this
                parameter lists the intra-group rankings,
                i.e. the users within the group