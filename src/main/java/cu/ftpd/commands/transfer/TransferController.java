package cu.ftpd.commands.transfer;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-08 - 18:39:36
 * @version $Id: TransferController.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface TransferController {
    public void error(Exception e, long bytesTransferred, long transferTime);
    public void complete(long bytesTransferred, long transferTime);
    public boolean isRunning();
    public void start();

    /**
     * Gets speed in kilobites per second as a long.
     * @return speed in kilobites per second as a long.
     */
    public long getSpeed();
}
