package cu.ftpd.commands.transfer;

import java.io.*;
import java.nio.CharBuffer;

/**
 * @author captain
 * @version $Id: TranslatingCharacterTransferThread.java 293 2009-03-04 20:07:38Z jevring $
 * @since 2008-nov-07 - 22:58:45
 */
public class TranslatingCharacterTransferThread extends CharacterTransferThread {
    protected final boolean outputToDisk;
    protected final String eol;

    /**
     * Is now appropriately fast now that we require Buffered* for the streams.
     *
     * @param controller the controller that we use for the callback when we are done.
     * @param in the source of the data.
     * @param out the target.
     * @param outputToDisk true of the <code>out</code> parameter represents a dikt writer of any sort. This causes the EOL used to be the line.separator system property.
     */
    public TranslatingCharacterTransferThread(TransferController controller, BufferedReader in, BufferedWriter out, boolean outputToDisk) {
        super(controller, in, out);
        this.outputToDisk = outputToDisk;
        if (outputToDisk) {
            eol = System.getProperty("line.separator");
        } else {
            eol = "\r\n";
        }
    }

    public void transfer() throws IOException {

        boolean lastWasR = false;
        int c;
        while ((c = in.read()) != -1) {
            bytesTransferred++;

            // \n == newline
            // \r\n == newline
            // \r == newline
            if (c == '\n') {
                if (!lastWasR) {
                    out.write(eol);
                } // otherwise we've sent the newline already
                lastWasR = false;
            } else if (c == '\r') {
                out.write(eol);
                lastWasR = true;
            } else {
                out.write(c);
                lastWasR = false;
            }
        }
        out.flush();
    }
}
