/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.commands.site;

import cu.ftpd.Connection;
import cu.ftpd.FtpdSettings;
import cu.ftpd.commands.site.actions.*;
import cu.ftpd.commands.site.actions.Shutdown;
import cu.ftpd.user.userbases.actions.*;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-20 : 15:34:32
 * @version $Id: SiteCommandHandler.java 286 2008-12-06 00:36:20Z jevring $
 */
public class SiteCommandHandler {
    private final Map<String, Action> actions = new HashMap<String, Action>(75, 1.0f);

    public SiteCommandHandler(FtpdSettings settings) {

        // BASIC
        actions.put("xdupe", new Xdupe());
        actions.put("shutdown", new Shutdown());
        actions.put("commandlog", new CommandLog());

        actions.put("who", new Who());
        actions.put("xwho", new XWho());
        actions.put("help", new Help(actions));
        actions.put("idle", new Idle(settings.get("/main/idle_timeout")));
        actions.put("kick", new Kick());
        actions.put("time", new Time());
        actions.put("rules", new Echo("rules", settings.get("/filesystem/rules")));
        actions.put("uptime", new Uptime());
        actions.put("welcome", new Echo("welcome", settings.get("/filesystem/welcome_msg")));

        Action vers = new Version();
        actions.put("vers", vers);
        actions.put("version", vers);
    }

    public void execute(Connection connection, User user, FileSystem fs, String parameters) {
        Action action = actions.get("help");
        boolean error;
        // OBS! this AIOOBE takes care of trying commands with the wrong number of parameters
        try {
            if (parameters != null) {
                String[] parameterList = parameters.split("\\s+");
                action = actions.get(parameterList[0].toLowerCase());
                if (action != null) {
                    action.execute(parameterList, connection, user, fs);
                } else {
                    noSuchCommand(connection, parameters);
                }
                error = false;
            } else {
                error = true;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            error = true;
        }
        if (error) {
            action.help(true, connection, fs);
        }
    }

    protected void noSuchCommand(Connection connection, String parameters) {
        connection.respond("500 Unknown command: " + parameters);
    }

    /**
     * Registers actions so that they become accessible via the site command system.
     * Actions registered are accessible by doing "site command" via an ftp session,
     * where "command" is the command used to register the action.
     * @param command the site command
     * @param action the action to be associated with the site command.
     * @return the previous action associated with this command, or <code>null</code> if no such association existed.
     * @see java.util.Map#put(Object, Object)
     */
    public Action registerAction(String command, Action action) {
        return actions.put(command, action);
    }

    /**
     * Removes a registered action from the set of possible site commands.
     * NOTE: This does NOT stop the action. This is the responsibility of the calling methos.
     * @param command the command used to invoke the action. For example "search" for the "site search" command.
     * @return The removed action, or <code>null</code> if no action was registered using that command.
     */
    public Action deregisterAction(String command) {
        return actions.remove(command);
    }

	/**
	 * Used by extending classes to register their own types of commands.
	 * @param name the name value
	 * @param path the path value
	 * @param type the type value (will not be "java" or "shell")
	 * @param addResponseCode the addResponseCode value
	 */
	public void registerCustomAction(String name, String path, String type, boolean addResponseCode) {
		// all subclasses should implement this
		throw new UnsupportedOperationException();
	}
}
