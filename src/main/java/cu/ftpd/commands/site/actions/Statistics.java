/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.commands.site.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.statistics.StatisticsEntry;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-27 : 03:44:59
 * @version $Id: Statistics.java 262 2008-10-30 21:29:48Z jevring $
 */
public class Statistics extends Action {
    private final int statistics;
    private final boolean groupStatistics;
    private static final String lineFormat = "200- %-1s %8s %-16s %-10s %-7s %-10s %-15s %-1s";

    public Statistics(int statistics, boolean groupStatistics, String name) {
        super(name);
        this.statistics = statistics;
        this.groupStatistics = groupStatistics;
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.STATISTICS)) {
            String number = getParameter(parameterList, "number");
            int max = 10;
            if (number != null) {
                try {
                    max = Integer.parseInt(number);
                } catch (NumberFormatException e) {
                    connection.respond("500 " + number + " was not a number");
                    return;
                }
            }
            String section = getParameter(parameterList, "section");
            if (section == null) {
                section = "default";
            }
            boolean raw = Boolean.parseBoolean(getParameter(parameterList, "raw"));
            if (groupStatistics) {
                // if we specify which group we're talking about, then show the internal ranking for that group.
                // if we don't specify a group, show the ranking for all the groups
                // allow specification of how many we want to list
                // NOTE: we need to split this up, as one will be returning a map of GroupStatisticsEntry, and the other will be returning a map of StatisticsEntry objects

                // accepts 'group', 'number' and 'raw'
                String group = getParameter(parameterList, "group");
                // group == null means show all groups (or group standing rather)
                report(connection, ServiceManager.getServices().getUserStatistics().getGroupStatistics(group, statistics, section), "Group", max, raw);
            } else {
                // accepts 'section', 'number' and 'raw'
                report(connection, ServiceManager.getServices().getUserStatistics().get(statistics, section), "User", max, raw);
            }
        } else {
            connection.respond("531 Permission denied.");
        }
    }

    private void report(Connection connection, TreeMap<Long, StatisticsEntry> map, String entityName, int max, boolean raw) {
        int i = 0;
        if (raw) {
            connection.respond("200- site " + this.name + " raw (" + entityName + ";section;files;size;time)");
            for (Map.Entry<Long, StatisticsEntry> entry : map.entrySet()) {
	            long files = entry.getValue().get(statistics + 1 /* _FILES */);
	            long bytes = entry.getValue().get(statistics /* _SIZE */);
	            long time = entry.getValue().get(statistics + 2/* _TIME */);
	            connection.respond(
                        entry.getValue().getName() + ";" +
                        escape(entry.getValue().getSection()) + ";" +
		                        files + ";" +
		                        bytes + ";" +
		                        time);
                if (++i == max) {
                    break;
                }
            }
            connection.respond("200 site " + this.name + " raw");
        } else {
            connection.respond(createHeader());
            connection.respond(String.format(lineFormat, Formatter.getBar(), "Position", entityName, "Section", "Files", "Size", "Speed", Formatter.getBar()));
            connection.respond(Formatter.createLine(200));
            int pos = 1;
            for (StatisticsEntry se : map.values()) {
	            long files = se.get(statistics + 1);
	            long bytes = se.get(statistics);
	            long time = se.get(statistics + 2 /* _TIME */);
	            if (files == 0) {
		            // stop once we get to people who haven't done anything
		            break;
	            }

	            
	            connection.respond(String.format(lineFormat,
                                                 Formatter.getBar(),
                                                 pos,
                                                 se.getName(),
                                                 se.getSection(),
                                                 files,
                                                 Formatter.size(bytes),
                                                 Formatter.speed(bytes, time),
                                                 Formatter.getBar()));
                if (++i == max) {
                    break;
                }
                pos++;
            }
            connection.respond(Formatter.createFooter(200));
        }
    }
}
