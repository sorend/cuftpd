/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.commands.site.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.text.MessageFormat;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:42:35
 * @version $Id: Kick.java 262 2008-10-30 21:29:48Z jevring $
 */
public class Kick extends Action {
    // kicked_user, kicker, kicker_group
    private final MessageFormat kick = new MessageFormat("KICK: \"{0}\" \"{1}\" \"{2}\"");

    public Kick() {
        super("kick");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.KICK)) {
            String id = getParameter(parameterList, "id");
            String reason = getParameter(parameterList, "reason");
            if (reason == null) {
                reason = "kicked";
            }
            if (id != null) {
                try {
                    long connectionId = Long.parseLong(id);
                    String kickedUser = Server.getInstance().kick(connectionId, reason);
                    Logging.getEventLog().log(kick.format(new String[] {kickedUser, user.getUsername(), user.getPrimaryGroup()}));
                    connection.respond("200 Connection " + id + " kicked");
                } catch (NumberFormatException e) {
                    connection.respond(id + "is not a number");
                }
            } else {
                Server.getInstance().kick(parameterList[1], reason); // username
                Logging.getEventLog().log(kick.format(new String[] {parameterList[1], user.getUsername(), user.getPrimaryGroup()}));
                connection.respond("200 User " + parameterList[1] + " kicked.");
            }
        } else {
            connection.respond("531 Permission denied.");
        }
    }
}
