/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.commands.site.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-nov-16 : 00:04:35
 * @version $Id: Chown.java 280 2008-11-24 18:52:18Z jevring $
 */
public class Chown extends Action {
    public Chown() {
        super("chown");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.FILES)) {
            // chown filename user group
            try {

                String filename;
                String username;
                String groupname;
                boolean recursive = false;
                if ("-R".equalsIgnoreCase(parameterList[1])) {
                    recursive = true;
                    filename = parameterList[2];
                    username = parameterList[3];
                    groupname = parameterList[4];
                } else {
                    filename = parameterList[1];
                    username = parameterList[2];
                    groupname = parameterList[3];
                }
                // actually, there's no reason to check if the user is a member of the group, it's perfectly acceptable to chown to a user:group
                // combo where the user is not a member of the group
                // but we do have to check that the user exists
                fs.chown(filename, username, groupname, recursive);
                connection.respond("200 Ownership of " + filename + " changed to " + username + ':' + groupname);
            } catch (NoSuchGroupException e) {
                // NOTE: this is a way to check for users, but anybody with FILES access probably also has USERS, so...
                connection.respond("500 " + e.getMessage());
            } catch (NoSuchUserException e) {
                connection.respond("500 " + e.getMessage());
            }
        } else {
            connection.respond("531 Permission denied");
        }
    }
}
