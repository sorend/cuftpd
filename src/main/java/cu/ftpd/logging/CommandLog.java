/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.logging;

import cu.ftpd.user.User;

import java.io.*;
import java.util.Map;
import java.util.Locale;
import java.util.Date;
import java.util.HashMap;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.net.InetAddress;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version $Id: CommandLog.java 268 2008-10-30 22:24:30Z jevring $
 * @since 2008-okt-04 - 23:37:40
 */
public class CommandLog {
    private int fileMode;
    private int verbosity;
    private File directory;
    private PrintWriter singleLog;
    private Map<String, PrintWriter> usernameLogs = new HashMap<String, PrintWriter>();
    private Map<Long, PrintWriter> connectionIdLogs = new HashMap<Long, PrintWriter>();
    private final DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy", Locale.ENGLISH);

    protected CommandLog(){} // does nothing, for inheriting classes only

    public CommandLog(int verbosity, int fileMode, String directory) throws FileNotFoundException {
        this.fileMode = fileMode;
        this.verbosity = verbosity;
        this.directory = new File(directory);
	    this.directory.mkdirs();
	    if (fileMode == 1) {
		    singleLog = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(this.directory, "commandlog.log"), true)), true);
	    }
    }

    public void logCommand(User user, long connectionId, InetAddress remoteHost, String command) {
        /*
            0. Off
            1. Log commands but not responses
            2. Log both commands AND responses
         */
        if (verbosity == 1 || verbosity == 2) {
            log(user, connectionId, remoteHost, command, true);
        }
    }

    public void logResponse(User user, long connectionId, InetAddress remoteHost, String response) {
        if (verbosity == 2) {
            log(user, connectionId, remoteHost, response, false);
        }
    }

    private void log(User user, long connectionId, InetAddress remoteHost, String message, boolean isCommand) {
        /*
            1. One file for everything
            2. One file per username
            3. One file per username and connectionId
         */
        PrintWriter log;
        if (fileMode == 1) {
            log = singleLog;
        } else if (fileMode == 2) {
            log = usernameLogs.get(user.getUsername());
            if (log == null) {
                try {
                    log = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(this.directory, user.getUsername() + ".log"), true)));
                    usernameLogs.put(user.getUsername(), log);
                } catch (FileNotFoundException e) {
                    Logging.getErrorLog().reportCritical("Failed to create username log for writing: " + e.getMessage());
                    return;
                }
            }
        } else if (fileMode == 3) {
            log = connectionIdLogs.get(connectionId);
            if (log == null) {
                try {
                    // note: this log should not append, since that will break things when we restart
                    log = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(this.directory, String.valueOf(new Date()).replaceAll(":", ".") + "." + String.valueOf(connectionId) + ".log"))));
                    connectionIdLogs.put(connectionId, log);
                } catch (FileNotFoundException e) {
                    Logging.getErrorLog().reportCritical("Failed to create connectionId log for writing: " + e.getMessage());
                    return;
                }
            }
        } else {
            return;
        }
        // responses will already have been split up in lines by the caller
        // time:id:username:remotehost:C|R:messageline
        if (message.startsWith("PASS")) {
            message = "PASS ******";
        }
        log.println(time.format(new Date()) + ":" + connectionId + ":" + (user == null ? "<unknown user>" : user.getUsername()) + ":" + remoteHost + (isCommand ? ":C:" : ":R:") + message);
        log.flush();
    }

    public void shutdown() {
        if (fileMode == 1) {
            singleLog.close();
        } else if (fileMode == 2) {
            for (PrintWriter pw : usernameLogs.values()) {
                pw.close();
            }
        } else if (fileMode == 3) {
            for (PrintWriter pw : connectionIdLogs.values()) {
                pw.close();
            }
        }
    }
}
