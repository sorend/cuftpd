/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.ServiceManager;
import cu.ftpd.Sort;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:43:10
 * @version $Id: Seen.java 305 2011-02-07 18:33:18Z jevring $
 */
public class Seen extends UserbaseAction {
    public Seen() {
        super("seen");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (parameterList.length == 1) {
            connection.respond("200 User " + user.getUsername() + " was last seen at " + Formatter.datetime(user.getLastlog()));
        } else if (user.hasPermission(UserPermission.SEEN)) {
            try {
                if ("*".equals(parameterList[1])) {
	                for (User u : Sort.users(ServiceManager.getServices().getUserbase().getUsers().values())) {
                        printUser(u, connection);
                    }
                } else {
                    User u = ServiceManager.getServices().getUserbase().getUser(parameterList[1]);
                    printUser(u, connection);
                }
            } catch (NoSuchUserException e) {
                // do NOT tell the user that the user didn't exist!
                connection.respond("500 User has not been online yet.");
            }
        } else {
            connection.respond("531 Permission denied.");
        }
    }

    private void printUser(User u, Connection connection) {
        if (u.getLastlog() == 0) {
            connection.respond("200 User " + u.getUsername() + " has never been online");
        } else {
            connection.respond("200 User " + u.getUsername() + " was last seen at " + Formatter.datetime(u.getLastlog()) + " (" + Formatter.shortDuration((long) ((double)(System.currentTimeMillis() - u.getLastlog()) / 1000.0d)) + " ago)");
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
