/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-dec-07 : 00:24:13
 * @version $Id: Permissions.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Permissions extends UserbaseAction {


    public Permissions() {
        super("permissions");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        connection.respond("200- Permissions:");
        connection.respond("200- UserPermission.SHUTDOWN = " + UserPermission.SHUTDOWN);
        connection.respond("200- UserPermission.ADDUSER = " + UserPermission.ADDUSER);
        connection.respond("200- UserPermission.DELUSER = " + UserPermission.DELUSER);
        connection.respond("200- UserPermission.VIEWUSER = " + UserPermission.VIEWUSER);
        connection.respond("200- UserPermission.UNDUPE = " + UserPermission.UNDUPE);
        connection.respond("200- UserPermission.FILES = " + UserPermission.FILES);
        connection.respond("200- UserPermission.USEREDIT = " + UserPermission.USEREDIT);
        connection.respond("200- UserPermission.CREDITS = " + UserPermission.CREDITS);
        connection.respond("200- UserPermission.WHO = " + UserPermission.WHO);
        connection.respond("200- UserPermission.UPTIME = " + UserPermission.UPTIME);
        connection.respond("200- UserPermission.KICK = " + UserPermission.KICK);
        connection.respond("200- UserPermission.RESCAN = " + UserPermission.RESCAN);
        connection.respond("200- UserPermission.TRAFFIC = " + UserPermission.TRAFFIC);
        connection.respond("200- UserPermission.USERS = " + UserPermission.USERS);
        connection.respond("200- UserPermission.SEEN = " + UserPermission.SEEN);
        connection.respond("200- UserPermission.GROUPS = " + UserPermission.GROUPS);
        connection.respond("200- UserPermission.STATISTICS = " + UserPermission.STATISTICS);
        connection.respond("200- UserPermission.SEE_HIDDEN = " + UserPermission.SEE_HIDDEN);
        connection.respond("200- UserPermission.PRIVILEGED = " + UserPermission.PRIVILEGED);
        connection.respond("200- Use \"site addpermissions\" and \"site delpermissions\" to");
        connection.respond("200- change the permissions for a user");
        connection.respond("200 End of permissions.");
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
