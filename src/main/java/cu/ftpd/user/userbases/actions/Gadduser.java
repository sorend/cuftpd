/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.userbases.UserExistsException;
import cu.ftpd.user.userbases.GroupLimitReachedException;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:42:03
 * @version $Id: Gadduser.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Gadduser extends UserbaseAction {
    public Gadduser() {
        super("gadduser");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        //      0       1           2           3           4++
        // site adduser group       username    password    ident@ip
        if (parameterList[1] == null || "".equals(parameterList[1])) {
            connection.respond("500 Must provide a groupname");
        } else if (parameterList[2] == null || "".equals(parameterList[2])) {
            connection.respond("500 Must provide a username");
        } else if (parameterList[3] == null || "".equals(parameterList[3])) {
            connection.respond("500 Must provide a password");
        } else if (!parameterList[1].matches("[\\w-]+")) {
            connection.respond("500 Groupname contains illegal characters.");
        } else if (!parameterList[2].matches("[\\w-]+")) {
            connection.respond("500 Username contains illegal characters.");
        } else {
            try {
                User newuser;
                if (user.hasPermission(UserPermission.ADDUSER)) {
                    newuser = ServiceManager.getServices().getUserbase().gAddUser(parameterList[1], parameterList[2], parameterList[3], false);
                } else if (user.isGadminForGroup(parameterList[1])) {
                    newuser = ServiceManager.getServices().getUserbase().gAddUser(parameterList[1], parameterList[2], parameterList[3], true);
                } else {
                    connection.respond("531 Permission denied.");
                    return;
                }
                // add ips if there are any. A user with no ips cannot log in
                String wrongIps = "";
                for (int i = 4; i < parameterList.length; i++) {
                    if (ip.matcher(parameterList[i]).matches()) {
                        newuser.addIp(parameterList[i]);
                    } else {
                        wrongIps += parameterList[i] + ' ';
                    }
                }
                connection.respond("200 User " + parameterList[2] + " successfully added." + (wrongIps.length() > 0 ? " The following ips were wrong: " + wrongIps : ""));
            } catch (IllegalArgumentException e) {
                // password != null || ""
                connection.respond("500 " + e.getMessage());
            } catch (UserExistsException e) {
                connection.respond("500 " + e.getMessage());
            } catch (NoSuchGroupException e) {
                connection.respond("500 " + e.getMessage());
            } catch (GroupLimitReachedException e) {
                connection.respond("500 " + e.getMessage());
            }
        }

    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
