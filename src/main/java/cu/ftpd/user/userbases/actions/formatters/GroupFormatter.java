/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions.formatters;

import cu.ftpd.Sort;
import cu.ftpd.logging.Formatter;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.statistics.StatisticsEntry;
import cu.ftpd.user.statistics.UserStatistics;
import cu.ftpd.ServiceManager;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-01 : 17:06:31
 * @version $Id: GroupFormatter.java 258 2008-10-26 12:47:23Z jevring $
 */
public class GroupFormatter {
    private static final ArrayList<String> format = new ArrayList<String>();

    public GroupFormatter(File formatterTemplateFile) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(formatterTemplateFile), "ISO-8859-1"));
            String line;
            while ((line = in.readLine()) != null) {
                format.add(line);
            }
        } catch (IOException e) {
            Logging.getErrorLog().reportError("Error reading group template file: " + e.getClass() + ":" + e.getMessage());
            format.clear(); // this causes the default format to be used
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String format(Group group) {

        if (format.isEmpty()) {
            return getDefaultFormat(group);
        } else {
            return getTemplateFormat(group);
        }
    }


    private String getTemplateFormat(Group group) {
        long allupbytes = 0;
        long alldnbytes = 0;
        long allupfiles = 0;
        long alldnfiles = 0;
        long alluptime = 0;
        long alldntime = 0;
        int usersInGroup = 0;
        int usersInGroupWithAllotment = 0;
        int usersInGroupWithLeech = 0;
        StatisticsEntry use;
        List<User> members = new LinkedList<User>();

		for (User user : Sort.users(ServiceManager.getServices().getUserbase().getUsers().values())) {
            if (user.isMemberOfGroup(group.getName())) {
                usersInGroup++;
                if (ServiceManager.getServices().getUserbase().userUsesLeechSlotForGroup(user.getUsername(), group.getName())) {
                    usersInGroupWithLeech++;
                }
                if (ServiceManager.getServices().getUserbase().userUsesAllotmentSlotForGroup(user.getUsername(), group.getName())) {
                    usersInGroupWithAllotment++;
                }
                members.add(user);
                use = ServiceManager.getServices().getUserStatistics().getUserStatistics(user.getUsername(), "default");
                allupbytes += use.get(UserStatistics.ALLUP_BYTES);
                alldnbytes += use.get(UserStatistics.ALLDN_BYTES);
                allupfiles += use.get(UserStatistics.ALLUP_FILES);
                alldnfiles += use.get(UserStatistics.ALLDN_FILES);
                alluptime += use.get(UserStatistics.ALLUP_TIME);
                alldntime += use.get(UserStatistics.ALLDN_TIME);
                // users with leech and allotment need to check if they are using up slots in this group for that
                // NOTE: these are the ONLY places where these methods are used. it seems a bit superflous.
                // they might also be used when listing allotments and leechers (no, then we'll just list the data from the objects, or will we?)
            }
        }

        // note: this COULD be simplified, but it would be a heck of a lot less readable. even though we don't use any loops
        StringBuilder sb = new StringBuilder(2000);
        sb.append(format.get(0)).append("\r\n");
        sb.append(String.format(format.get(1), group.getName())).append("\r\n");
        sb.append(String.format(format.get(2), group.getDescription())).append("\r\n");
        sb.append(String.format(format.get(3), usersInGroup)).append("\r\n");
        sb.append(String.format(format.get(4), group.getSlots())).append("\r\n");
        sb.append(String.format(format.get(5), usersInGroupWithLeech + "/" + group.getLeechSlots())).append("\r\n");
        sb.append(String.format(format.get(6), usersInGroupWithAllotment + "/" + group.getAllotmentSlots())).append("\r\n");
        sb.append(String.format(format.get(7), Formatter.size(group.getMaxAllotment()))).append("\r\n");
        sb.append(format.get(8)).append("\r\n");
        sb.append(format.get(9)).append("\r\n");
        sb.append(format.get(10)).append("\r\n");
        sb.append(String.format(format.get(11), allupfiles, alldnfiles)).append("\r\n");
        sb.append(String.format(format.get(12), Formatter.size(allupbytes), Formatter.size(alldnbytes))).append("\r\n");
        sb.append(String.format(format.get(13), Formatter.speed(allupbytes, alluptime), Formatter.speed(alldnbytes, alldntime))).append("\r\n");
        sb.append(format.get(14)).append("\r\n");
        sb.append(format.get(15)).append("\r\n");
        sb.append(format.get(16)).append("\r\n");
        for (User gadmin : members) {
            sb.append(String.format(format.get(17), gadmin.getUsername(), gadmin.isGadminForGroup(group.getName()))).append("\r\n");
        }
        sb.append(format.get(18)).append("\r\n");
        return sb.toString();
    }

    private String getDefaultFormat(Group group) {
        return "200 " + group.getName();
    }
}
