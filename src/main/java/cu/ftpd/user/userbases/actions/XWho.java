/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.logging.Formatter;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.util.Set;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-28 - 19:50:30
 * @version $Id: XWho.java 312 2011-09-03 16:00:17Z jevring $
 */
public class XWho  extends UserbaseAction {
    // connectionId, username, idletime, command/idle
    private static final String lineFormat = "200- %8s %-16s %-12s %-40s %-40s";

    public XWho() {
        super("xwho");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.KICK)) {
            Set<Connection> conns = Server.getInstance().getConnections();
            connection.respond(String.format(lineFormat, "Id", "Username", "Idletime", "ident@host", "Command"));
            synchronized(conns) {
                for (Connection c : conns) {
                    User u = c.getUser();
                    if (c.isUploading() || c.isDownloading()) {
                        connection.respond(String.format(lineFormat, c.getConnectionId(), (u == null ? "n/a" : u.getUsername()), c.getIdleTime(), c.getIdent() + "@" + c.getHost(), c.getLastCommand() + " @ " + Formatter.speedFromKBps(c.getCurrentSpeed())));
                    } else {
                        connection.respond(String.format(lineFormat, c.getConnectionId(), (u == null ? "n/a" : u.getUsername()), c.getIdleTime(), c.getIdent() + "@" + c.getHost(), "Idle"));
                    }
                }
                connection.respond("200 xwho");
            }
         } else {
             connection.respond("531 Permission denied.");
         }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
