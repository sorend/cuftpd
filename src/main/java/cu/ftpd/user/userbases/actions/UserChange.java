/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:51:13
 * @version $Id: UserChange.java 281 2008-11-24 19:20:23Z jevring $
 */
public class UserChange extends UserbaseAction {
    public UserChange() {
        super("change");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        // _todo: careful when we use *, as we must get the group from it and allow it to be applied to all members of that group
        // _todo: also be careful about allotment and leech, since it has to match the number of slots
        // Only allow this for people with USEREDIT. that way we get out of having to worry about the
        // number of available slots
        // or maybe we shouldn't even implement it here. it's not like you'd want to set everybody's tagline to the same
        // yeah, we're skipping that functionality here.



        String username = parameterList[1];
        String property = parameterList[2];
        String value = parameterList[3];
        boolean hasUsereditPermission = user.hasPermission(UserPermission.USEREDIT);
        boolean currentUserIsGadminForGroup = false;
        String groupname = null;
        int loc;
        if ((loc = username.indexOf('@')) > -1) {
            groupname = username.substring(loc+1);
            username = username.substring(0, loc);
            currentUserIsGadminForGroup = user.isGadminForGroup(groupname);
        }
        try {
            User target = ServiceManager.getServices().getUserbase().getUser(username);
            if (!target.isMemberOfGroup(groupname) && groupname != null) {
                connection.respond("500 User " + username + " is not a member of group " + groupname);
                return;
            }
            boolean ok;
            if (hasUsereditPermission) {
                // here we do all the things that can only be done with full USEREDIT permissions
                // NOTE: we keep "site primarygroup" and "site tagline" and "site passwd" as a convenience command for users to change their own information
                if ("logins".equals(property)) {
                    target.setLogins(Integer.parseInt(value));
                    connection.respond("200 Setting 'logins' to " + target.getLogins() + " for user " + target.getUsername());
                } else if ("passwd".equals(property)) {
                    target.passwd(ServiceManager.getServices().getUserbase().createHashedPassword(value));
                    connection.respond("200 changed password for user " + target.getUsername());
                } else if ("suspended".equals(property)) {
                    target.setSuspended(Boolean.parseBoolean(value));
                    connection.respond("200 Setting 'suspended' to " + target.isSuspended() + " for user " + target.getUsername());
                } else if ("hidden".equals(property)) {
                    target.setHidden(Boolean.parseBoolean(value));
                    connection.respond("200 Setting 'hidden' to " + target.isHidden() + " for user " + target.getUsername());
                } else if ("tagline".equals(property)) {
                    target.setTagline(Formatter.join(parameterList, 3, parameterList.length, " "));
                    connection.respond("200 Setting 'tagline' to \"" + target.getTagline() + "\" for user " + target.getUsername());
                } else if ("primarygroup".equals(property)) {
                    if (target.isMemberOfGroup(value)) {
                        target.setPrimaryGroup(value);
                        connection.respond("200 Setting 'primarygroup' to " + target.getPrimaryGroup() + " for user " + target.getUsername());
                    } else {
                        connection.respond("500 User " + target.getUsername() + " is not a member of group " + value);
                    }
                } else if ("leech".equals(property)) {
                    // we don't care about the return value here, it will always succeed if we don't check for availability
                    /*
                    [19:16:49] site change captain leech true
                    [19:16:49] 200 Setting 'leech' to true for user captain
                    [19:17:01] site change captain leech true
                    [19:17:01] 200 Setting 'leech' to true for user captain
                    [19:17:06] site change captain leech false
                    [19:17:06] 200 Setting 'leech' to true for user captain
                    [19:17:13] site change captain leech false
                    [19:17:13] 200 Setting 'leech' to false for user captain


                     */
                    ServiceManager.getServices().getUserbase().setLeechForUser(Boolean.parseBoolean(value), username, groupname, false);
                    connection.respond("200 Setting 'leech' to " + target.hasLeech() + " for user " + target.getUsername());
                } else if ("allotment".equals(property)) {
                    // No need to check for NumberFormatException here, that is done in an outer try
                    final long credits = Formatter.size(value);
                    ServiceManager.getServices().getUserbase().setAllotmentForUser(credits, username, groupname, false);
                    connection.respond("200 Setting 'allotment' to " + credits + " for user " + target.getUsername());
                    // we don't care about the return value here, it will always succeed if we don't check for availability
                } else {
                    help(true, connection, fs);
                }
            } else if (currentUserIsGadminForGroup) {
                if ("leech".equals(property)) {
                    ok = ServiceManager.getServices().getUserbase().setLeechForUser(Boolean.parseBoolean(value), username, groupname, true);
                    if (ok) {
                        connection.respond("200 Setting 'leech' to " + target.hasLeech() + " for user " + target.getUsername() + " in group " + groupname);
                    } else {
                        connection.respond("500- Failed to set 'leech' to true for user " + target.getUsername() + " in group " + groupname);
                        connection.respond("500  because the group uses all its allocated leech slots already");
                    }
                } else if ("allotment".equals(property)) {
                    // No need to check for NumberFormatException here, that is done in an outer try
                    long credits = Formatter.size(value);
                    ok = ServiceManager.getServices().getUserbase().setAllotmentForUser(credits, username, groupname, true);
                    if (ok) {
                        connection.respond("200 Setting 'allotment' to " + credits + " for user " + target.getUsername() + " in group " + groupname);
                    } else {
                        connection.respond("500- Failed to set 'allotment' to " + credits + " for user " + target.getUsername() + " in group " + groupname);
                        connection.respond("500  because the group uses all its allotment slots already, or the allotment was larger than the max allowed allotment");
                    }
                } else {
                    help(true, connection, fs);
                }
            } else {
                connection.respond("531 Permission denied.");
            }
        } catch (NumberFormatException e) {
            connection.respond("500 " + value + " is not a number");
        } catch (NoSuchUserException e) {
            connection.respond("500 " + e.getMessage());
        } catch (NoSuchGroupException e) {
            connection.respond("500 " + e.getMessage());
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
