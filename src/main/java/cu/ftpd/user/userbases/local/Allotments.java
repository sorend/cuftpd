/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.local;

import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Logging;

import java.io.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class keeps track of allotments.
 * It contains 4-tuples with { username, group, allotment, last_allotment_time }.
 * There can be multiple allotment slots per username, since the user can have allotments from different groups, including the general 'group'.
 *
 * The class is backed by <code>data/allotment.conf</code>, which gets updated every time something in the class changes.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-sep-26 : 18:02:16
 * @version $Id: Allotments.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Allotments {
    private final File file;
    private final List<Allotment> allotments = Collections.synchronizedList(new LinkedList<Allotment>());
    private long lastAllotment = 0;

    public Allotments(File file) throws IOException {
        this.file = file;
        synchronized(allotments) {
            BufferedReader in = null;
            if (!file.exists()) {
                System.out.println("allotments.conf not found, creating...");
                if (!file.createNewFile()) {
                    throw new IOException("Unable to create new allotments.conf");
                }
                return; // we might as well return here since we don't want to read an empty file anyway
            }
            try {

                // NOTE: don't move this to the user object, since the user object doesn't know about the userdir
                in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String line;
                while((line = in.readLine()) != null) {
                    if (line.startsWith("#")) {
                        continue; // this was a comment
                    }
                    if (line.startsWith("LAST")) {
                        String[] ss = line.split("=",2);
                        try {
                            lastAllotment = Long.parseLong(ss[1]);
                        } catch (NumberFormatException e) {
                            lastAllotment = 0;
                        }
                        continue;
                    }
                    String[] lp = line.split(";", 4);
                    // username, group, allotment, last_allotment_time
                    // NOTE: while there are currently only 3 fields for each allotment, we keep trying to look for 4,
                    // as there used to be 4, and we don't want to break people's files.
                    // this should be able to change in a couple of weeks when people have updated, since we write the
                    // proper format now
                    if (lp.length == 3 || lp.length == 4) {
                        allotments.add(new Allotment(lp[0], lp[1], Long.parseLong(lp[2])));
                    } else {
                        // something wrong with the line
                        System.err.println("Skipping erronenous allotment line: " + line);
                    }
                }
            } catch (IOException e) {
                Logging.getErrorLog().reportCritical("Error while loading allotments: " + e.getMessage());
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void save() {
        synchronized(allotments) {
            FileSystem.fastBackup(file);
            PrintWriter out = null;
            try {
                out = new PrintWriter(new FileOutputStream(file));
                out.println("LAST="+lastAllotment);
                for (Allotment a : allotments) {
                    out.println(a.toString());
                }
                out.flush();
            } catch (FileNotFoundException e) {
                Logging.getErrorLog().reportError("Failed to write allotments to disk: " + e.getMessage());
            } finally {
                if (out != null) {
                    out.close();
                }
            }
        }
    }

    public void addAllotment(Allotment allotment) {
        allotments.add(allotment);
    }

    /**
     * Removes an allotment entry for the specified user and group.
     *
     * @param username the username that is being alloted credits.
     * @param groupname null indicates the whole site.
     */
    public void removeAllotment(String username, String groupname) {
        synchronized(allotments) {
            Iterator<Allotment> i = allotments.iterator();
            while (i.hasNext()) {
                Allotment allotment = i.next();
                if ((allotment.getGroupname().equalsIgnoreCase(groupname) || groupname == null) && allotment.getUsername().equalsIgnoreCase(username)) {
                    i.remove();
                }
            }
        }
    }

    public void removeAllAllotmentsForGroup(String groupname) {
        removeAllAllotments(groupname, true);
    }
    public void removeAllAllotmentsForUser(String username) {
        removeAllAllotments(username, false);
    }
    private void removeAllAllotments(String name, boolean group) {
        Iterator<Allotment> i = allotments.iterator();
        synchronized(allotments) {
            while (i.hasNext()) {
                Allotment allotment = i.next();
                if ((group && allotment.getGroupname().equalsIgnoreCase(name)) || (!group && allotment.getUsername().equalsIgnoreCase(name))) {
                    i.remove();
                }
            }
        }
    }

    /**
     * Returns a list of all the allotments.
     * NOTE: when iterating over this, synchronize on the object that is returned, which is the mutex for the list itself.
     * @return the list of allotments.
     */
    public List<Allotment> getAllotments() {
        return allotments;
    }

    public List<Allotment> getAllotmentsForGroup(String groupname) {
        List<Allotment> l = new LinkedList<Allotment>();
        synchronized(allotments) {
            for (Allotment a : allotments) {
                if (a.getGroupname().equalsIgnoreCase(groupname)) {
                    l.add(a);
                }
            }
            return l;
        }
    }

    public boolean userUsesAllotmentSlotForGroup(String username, String groupname) {
        synchronized(allotments) {
            for (Allotment a : allotments) {
                if (a.getUsername().equals(username) && (a.getGroupname().equalsIgnoreCase(groupname) ||  groupname == null)) {
                    return true;
                }
            }
        }
        return false;
    }

    public long getLastAllotment() {
        return lastAllotment;
    }

    public void setLastAllotment(long lastAllotment) {
        this.lastAllotment = lastAllotment;
    }
}
