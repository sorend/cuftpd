package cu.ftpd.user.userbases.changetracking;

import javax.jms.MessageProducer;
import java.net.URI;

/**
 * @author markus@jevring.net
 */
class Peer {
    private final String name;
    private final URI broker;
    private final MessageProducer messageProducer;
    private final ChangeReceiver changeReceiver;

    Peer(String name, URI broker, MessageProducer messageProducer, ChangeReceiver changeReceiver) {
        this.name = name;
        this.broker = broker;
        this.messageProducer = messageProducer;
        this.changeReceiver = changeReceiver;
    }

    public String getName() {
        return name;
    }

    public URI getBroker() {
        return broker;
    }

    public MessageProducer getMessageProducer() {
        return messageProducer;
    }

    public ChangeReceiver getChangeReceiver() {
        return changeReceiver;
    }
}
