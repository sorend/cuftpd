package cu.ftpd.user.userbases.changetracking;

/**
 * @author markus@jevring.net
 */
public class GroupChange implements Change {
    public static enum Property {
        Slots,
        LeechSlots,
        AllotmentSlots,
        MaxAllotment,
        Description
    }

    private final Property property;
    private final String group;
    private final String parameter;
    private final String peer;

    public GroupChange(Property property, String group, String parameter, String peer) {
        this.property = property;
        this.group = group;
        this.parameter = parameter;
        this.peer = peer;
    }

    public GroupChange(Property property, String group, Object parameter) {
        this(property, group, String.valueOf(parameter), null);
    }

    public Property getProperty() {
        return property;
    }

    public String getGroup() {
        return group;
    }

    public String getParameter() {
        return parameter;
    }

    public String getPeer() {
        return peer;
    }

    @Override
    public String toString() {
        return "GroupChange{property=" + property + ", group=" + group + ", parameter=" + parameter + '}';
    }
}
