package cu.ftpd.user.userbases.changetracking;

import cu.ftpd.logging.Logging;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;

import javax.jms.*;
import java.io.File;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * @author markus@jevring.net
 */
public class AsynchronousMessageQueueChangeTracker implements ChangeTracker {
    private final ExecutorService connectionExecutor = Executors.newCachedThreadPool(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            // make the connecting threads demon threads, so they won't get in the way of shutting down
            t.setDaemon(true);
            return t;
        }
    });
    private final Map<String, Peer> peers = new ConcurrentHashMap<>();
    private final BrokerService brokerService;
    private final Session session;
    private final Connection connection;
    private final String myName;

    public AsynchronousMessageQueueChangeTracker(URI broker, String myName) throws Exception {
        this.myName = myName;
        this.brokerService = new BrokerService();
        this.brokerService.setDataDirectoryFile(new File("data/activemq-data"));
        this.brokerService.addConnector(broker);
        this.brokerService.setPersistent(true);
        this.brokerService.start();

        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(broker);
        connection = connectionFactory.createConnection();
        session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
    }

    public void stop() throws Exception {
        for (Peer peer : peers.values()) {
            try {
                peer.getChangeReceiver().stop();
            } catch (JMSException e) {
                Logging.getErrorLog().reportException("Shutdown failure for peer " + peer.getName(), e);
            }
        }
        session.close();
        connection.close();
        brokerService.stop();
        connectionExecutor.shutdownNow();
    }

    @Override
    public void addChange(Change change) {
        for (Peer peer : peers.values()) {
            try {
                final MessageProducer messageProducer = peer.getMessageProducer();
                if (peer.getName().equals(change.getPeer())) {
                    System.out.println("NOT propagating own change to '" + messageProducer.getDestination() + "': " + change);
                } else {
                    System.out.println("Sending change to '" + messageProducer.getDestination() + "': " + change);
                    messageProducer.send(session.createTextMessage(change.toString()));
                }
            } catch (JMSException e) {
                // todo: must handle this. it's unlikely, however, since we're running the brokerService as well.
                // that just means, I suppose, that we must shut down
                Logging.getErrorLog().reportException("Peer '" + peer.getName() + "' disconnected, attempting to reconnect", e);
            }
        }
    }

    public void addPeer(final String name, URI broker, ChangeApplicator changeApplicator) throws JMSException {
        final Queue queue = session.createQueue("cu.mq.userbase." + name);
        final MessageProducer producer = session.createProducer(queue);
        final ChangeReceiver changeReceiver = new ChangeReceiver(broker, name, myName, this, changeApplicator);
        final Peer peer = new Peer(name, broker, producer, changeReceiver);
        peers.put(name, peer);

        // "it used to work before", but for some reason, Connection.createSession() refuses to return until
        // the broker on the other side is up.
        // to prevent this from clogging up things forever, when the peer isn't actually online, we're going
        // to put the connection in a thread. we don't need a monitor thread, as it will automatically reconnect
        // with the "failover:" protocol
        connectionExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    changeReceiver.start();
                } catch (JMSException e) {
                    Logging.getErrorLog().reportException("Could not connect to peer '" + name + "'", e);
                }
            }
        });
    }
}
