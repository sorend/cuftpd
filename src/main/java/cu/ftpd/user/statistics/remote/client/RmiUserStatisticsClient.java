/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.statistics.remote.client;

import cu.ftpd.Server;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.statistics.StatisticsEntry;
import cu.ftpd.user.statistics.UserStatistics;
import cu.ftpd.user.statistics.remote.server.RmiUserStatistics;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.TreeMap;
import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-11 : 20:54:54
 * @version $Id: RmiUserStatisticsClient.java 280 2008-11-24 18:52:18Z jevring $
 */
public class RmiUserStatisticsClient implements UserStatistics {
    private final String host;
    private final int port;
    private final int retry;
    private final Object reconnectCompletedLock = new Object();
    private RmiUserStatistics rmi;
    private Registry registry;


    public RmiUserStatisticsClient(String host, int port, int retry) throws IOException {
        this.host = host;
        this.port = port;
        this.retry = retry;
        try {
            registry = LocateRegistry.getRegistry(host, port, new SslRMIClientSocketFactory());
            rmi = (RmiUserStatistics) registry.lookup("statistics");
            if (rmi.isUp()) {
                System.out.println("Located remote user statistics at " + host + ':' + port);
            } else {
                throw new IOException("Failed to connect to remote statistics. (Found registry but not statistics)");
            }
        } catch (Exception e) {
            throw new IOException("Failed to connect to remote statistics: " + e.getMessage(), e);
        }
        
    }

    public void upload(String username, String section, long bytes, long time) {
        try {
            rmi.upload(username, section, bytes, time);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            upload(username, section, bytes, time);
        }
    }

    public void download(String username, String section, long bytes, long time) {
        try {
            rmi.download(username, section, bytes, time);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            download(username, section, bytes, time);
        }
    }

    public StatisticsEntry getUserStatistics(String username, String section) {
        try {
            return rmi.getUserStatistics(username, section);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getUserStatistics(username, section);
        }
    }

    public void store(StatisticsEntry statisticsEntry) {
        throw new IllegalStateException("UserStatistics.store() should never be called remotely!");

    }

    public TreeMap<Long, StatisticsEntry> get(int statistics, String section) {
        try {
            return rmi.get(statistics, section);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return get(statistics, section);
        }
    }

    public TreeMap<Long, StatisticsEntry> getGroupStatistics(String groupname, int statistics, String section) {
        try {
            return rmi.get(groupname, statistics, section);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getGroupStatistics(groupname, statistics, section);
        }
    }


    public void deleteUser(String username) {
        try {
            rmi.deleteUser(username);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            deleteUser(username);
        }
    }

    private void reinitialize() {
        // _todo: Se till att vi inte har flera tradar som forsoker reconnecta userbase eller statistics nar dom gar ner, utan att det bara ar en som gor det.
        // done, in the sense that we will check 'reinitializing', which is volatile

        // _todo: spawn a new thread for this, so that if a user disconnects while we wait, it doesn't die?????
        // that's not needed. This thread keeps on going even if the user disconnects the socket

        // we can't use an if-statement. if we do, then two threads could examine the statment and enter before it is set.
        // ideally we want things waiting here for the lock to not have to reconnect after our recommection is done
        // but then again we can't be 100% sure that it didn't disconnect again
        synchronized (reconnectCompletedLock) {
            // suspend the server
            // try to re-connect the rmi
            // when re-connected, unsuspend the server
            // loop forever until re-connected

            // do a quick check here. if we entered just after it got fixed by some other thread, we don't have to do the whole dance again
            try {
                if (rmi.isUp()) {
                    return;
                }
            } catch (RemoteException e) {
                // this just means that it is actually down, continue to do the reconnect
            }

            long start = System.currentTimeMillis();
            Server.getInstance().suspend();
            int i = 0;
            while (true) {
                i++;
                Logging.getErrorLog().reportError("Trying to re-connect to remote statistics, tries: " + i);
                System.err.println("Trying to re-connect to remote statistics, tries: " + i);
                try {
                    registry = LocateRegistry.getRegistry(host, port, new SslRMIClientSocketFactory());
                    rmi = (RmiUserStatistics) registry.lookup("statistics");
                    if (rmi.isUp()) {
                        break; // if we get here, then we are reconnected (since it didn't throw any exceptions)
                    }
                } catch (RemoteException e) {
                } catch (NotBoundException e) {
                } catch (Exception e) {
                    Logging.getErrorLog().reportCritical("Something unexpected went wrong when trying to connect to the statistics: " + e.getClass() + ':' + e.getMessage());
                }
                try {
                    Thread.sleep(retry * 1000);
                } catch (InterruptedException e) {
                    System.err.println(e.getMessage());
                    Logging.getErrorLog().reportError(e.getMessage());
                }
            }
            Server.getInstance().unsuspend();
            System.err.println("Remote statistics reconnected. time taken: " + ((System.currentTimeMillis() - start) / 1000) + " seconds");
            Logging.getErrorLog().reportError("Remote statistics reconnected. time taken: " + ((System.currentTimeMillis() - start) / 1000) + " seconds");
        }
    }
/*
    private void reinitialize() {
        System.out.println("reinitializing in remote statistics");
        // suspend the server
        // try to re-connect the rmi
        // when re-connected, unsuspend the server
        // loop forever until re-connected
        Server.getInstance().suspend();
        while (true) {
            try {
                registry = LocateRegistry.getRegistry(host, port, new SslRMIClientSocketFactory());
                rmi = (RmiUserStatistics) registry.lookup("statistics");
                if (rmi.isUp()) {
                    break; // if we get here, then we are reconnected (since it didn't throw any exceptions)
                }
            } catch (RemoteException e) {
            } catch (NotBoundException e) {
            }
            try {
                Thread.sleep(retry * 1000);
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
        Server.getInstance().unsuspend();
        
    }
*/
}
