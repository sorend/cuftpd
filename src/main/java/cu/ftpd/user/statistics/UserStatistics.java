/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.statistics;

import java.util.TreeMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-11 : 20:54:15
 * @version $Id: UserStatistics.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface UserStatistics {
    public static final int ALLUP_BYTES =   0;
    public static final int ALLUP_FILES =   1;
    public static final int ALLUP_TIME =    2;
    public static final int ALLDN_BYTES =   3;
    public static final int ALLDN_FILES =   4;
    public static final int ALLDN_TIME =    5;
    public static final int MNUP_BYTES =   6;
    public static final int MNUP_FILES =   7;
    public static final int MNUP_TIME =    8;
    public static final int MNDN_BYTES =   9;
    public static final int MNDN_FILES =  10;
    public static final int MNDN_TIME =   11;
    public static final int WKUP_BYTES =  12;
    public static final int WKUP_FILES =  13;
    public static final int WKUP_TIME =   14;
    public static final int WKDN_BYTES =  15;
    public static final int WKDN_FILES =  16;
    public static final int WKDN_TIME =   17;
    public static final int DAYUP_BYTES = 18;
    public static final int DAYUP_FILES = 19;
    public static final int DAYUP_TIME =  20;
    public static final int DAYDN_BYTES = 21;
    public static final int DAYDN_FILES = 22;
    public static final int DAYDN_TIME =  23;

    // resolves number to string, should we need it, can be done with an array instead of a HashMap

    public void upload(String username, String section, long bytes, long time);

    public void download(String username, String section, long bytes, long time);

    public StatisticsEntry getUserStatistics(String username, String section);

    public void store(StatisticsEntry statisticsEntry);

    public TreeMap<Long, StatisticsEntry> get(int statistics, String section);
    public TreeMap<Long, StatisticsEntry> getGroupStatistics(String groupname, int statistics, String section);

    public void deleteUser(String username);
}
