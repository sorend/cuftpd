/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.statistics;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-28 : 19:38:31
 * @version $Id: StatisticsEntry.java 258 2008-10-26 12:47:23Z jevring $
 */
public class StatisticsEntry implements Serializable {
    private final String name;
    private final String section;
    private final long[] statistics = new long[24];
    private long lastUpdate;

    public StatisticsEntry(String name, String section) {
        this.name = name;
        this.section = section;
    }

    private void updateTime() {
        Calendar now = Calendar.getInstance();
        Calendar lastUpdateCalendar = Calendar.getInstance();
        lastUpdateCalendar.setTimeInMillis(lastUpdate);
        boolean updated = false;

        if (now.get(Calendar.MONTH) != lastUpdateCalendar.get(Calendar.MONTH)) {
            statistics[UserStatistics.MNUP_BYTES] = 0;
            statistics[UserStatistics.MNUP_FILES] = 0;
            statistics[UserStatistics.MNUP_TIME] = 0;
            statistics[UserStatistics.MNDN_BYTES] = 0;
            statistics[UserStatistics.MNDN_FILES] = 0;
            statistics[UserStatistics.MNDN_TIME] = 0;
            updated = true;
        }
        if (now.get(Calendar.WEEK_OF_YEAR) != lastUpdateCalendar.get(Calendar.WEEK_OF_YEAR)) {
            statistics[UserStatistics.WKUP_BYTES] = 0;
            statistics[UserStatistics.WKUP_FILES] = 0;
            statistics[UserStatistics.WKUP_TIME] = 0;
            statistics[UserStatistics.WKDN_BYTES] = 0;
            statistics[UserStatistics.WKDN_FILES] = 0;
            statistics[UserStatistics.WKDN_TIME] = 0;
            updated = true;
        }
        if (now.get(Calendar.DAY_OF_YEAR) != lastUpdateCalendar.get(Calendar.DAY_OF_YEAR)) {
            statistics[UserStatistics.DAYUP_BYTES] = 0;
            statistics[UserStatistics.DAYUP_FILES] = 0;
            statistics[UserStatistics.DAYUP_TIME] = 0;
            statistics[UserStatistics.DAYDN_BYTES] = 0;
            statistics[UserStatistics.DAYDN_FILES] = 0;
            statistics[UserStatistics.DAYDN_TIME] = 0;
            updated = true;
        }
        if (updated) {
            lastUpdate = System.currentTimeMillis();
        }
    }

    // since this is used purely in a read-only fashion when being requested from the remote, it's quite ok to make this lock transient, since these methods can't be called on an object of this type having been delivered remotely
    private final transient Object updateLock = new Object();
    public void upload(long bytes, long time) {
        synchronized(updateLock) {
            updateTime();
            statistics[UserStatistics.ALLUP_FILES]++;
            statistics[UserStatistics.ALLUP_BYTES]+= bytes;
            statistics[UserStatistics.ALLUP_TIME]+= time;
            statistics[UserStatistics.MNUP_FILES]++;
            statistics[UserStatistics.MNUP_BYTES]+= bytes;
            statistics[UserStatistics.MNUP_TIME]+= time;
            statistics[UserStatistics.WKUP_FILES]++;
            statistics[UserStatistics.WKUP_BYTES]+= bytes;
            statistics[UserStatistics.WKUP_TIME]+= time;
            statistics[UserStatistics.DAYUP_FILES]++;
            statistics[UserStatistics.DAYUP_BYTES]+= bytes;
            statistics[UserStatistics.DAYUP_TIME]+= time;
            lastUpdate = System.currentTimeMillis();
        }
    }
    public void download(long bytes, long time) {
        synchronized(updateLock) {
            updateTime();
            statistics[UserStatistics.ALLDN_FILES]++;
            statistics[UserStatistics.ALLDN_BYTES]+= bytes;
            statistics[UserStatistics.ALLDN_TIME]+= time;
            statistics[UserStatistics.MNDN_FILES]++;
            statistics[UserStatistics.MNDN_BYTES]+= bytes;
            statistics[UserStatistics.MNDN_TIME]+= time;
            statistics[UserStatistics.WKDN_FILES]++;
            statistics[UserStatistics.WKDN_BYTES]+= bytes;
            statistics[UserStatistics.WKDN_TIME]+= time;
            statistics[UserStatistics.DAYDN_FILES]++;
            statistics[UserStatistics.DAYDN_BYTES]+= bytes;
            statistics[UserStatistics.DAYDN_TIME]+= time;
            lastUpdate = System.currentTimeMillis();
        }
    }

    public String getName() {
        return name;
    }

    public String getSection() {
        return section;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public void set(int statistics, long value) {
        // NOTE: do NOT save here, since this is just used for initialization
        this.statistics[statistics] = value;
    }

    public long get(int statistics) {
        // no need to store after this updateTime(). whenever new file data comes in is when we store
        updateTime(); // not needed for AL, but needed for everything else, and in the case of AL it doesn't matter
        return this.statistics[statistics];
    }

    public String toString() {
        //updateTime(); // we don't need to call updateTime() here, since the method that uses this method, namely store() only stores right after an upload or download, and uploads and downloads call updateTime() already
        return Arrays.toString(statistics);
    }
}
