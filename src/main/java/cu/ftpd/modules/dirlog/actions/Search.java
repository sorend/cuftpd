/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.dirlog.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.modules.dirlog.DirlogEntry;
import cu.ftpd.modules.dirlog.Dirlog;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.util.LinkedList;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:39:40
 * @version $Id: Search.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Search extends Action {
    private final Dirlog dirlog;
    private static final String lineFormat = "200- %-1s %-5s %-9s %-13s %-41s %-1s";

    public Search(Dirlog dirlog) {
        super("search");
        this.dirlog = dirlog;
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (parameterList.length > 1) {
            boolean raw = parameterList[1].equalsIgnoreCase("-raw");
            String s;
            if (raw) {
                s = Formatter.join(parameterList, 2, parameterList.length, " ");
            } else {
                s = Formatter.join(parameterList, 1, parameterList.length, " ");
            }

            LinkedList<DirlogEntry> list = dirlog.search((s.startsWith("*") ? "" : ".*") + s.replace(".","\\.").replace(" ", ".+").replace("*", ".*") + (s.endsWith("*") ? "" : ".*"));
            if (raw) {
                connection.respond("200- SITE SEARCH RAW (files;size;time;name)");
                for (DirlogEntry de : list) {
                    // don't check .canSee() here, that's just for hiding users.
                    // if you don't want stuff from group dirs showing up here, disable dirlog for those dirs then
                    connection.respond(de.getFiles() + ";" + de.getSize() + ";" + de.getTime() + ";" + escape(de.getPath()));
                }
                connection.respond("200 SITE SEARCH RAW");
            } else {
                connection.respond(createHeader());
                connection.respond(String.format(lineFormat, Formatter.getBar(), "Files", "Size", "Age", "Directory", Formatter.getBar()));
                connection.respond(Formatter.createLine(200));
                for (DirlogEntry de : list) {
                    // don't check .canSee() here, that's just for hiding users.
                    // if you don't want stuff from group dirs showing up here, disable dirlog for those dirs then
                    connection.respond(String.format(lineFormat, Formatter.getBar(), de.getFiles(), Formatter.size(de.getSize()), Formatter.shortDuration((System.currentTimeMillis() - de.getTime()) / 1000), de.getPath(), Formatter.getBar()));
                }
                connection.respond(Formatter.createFooter(200));
            }
        } else {
            help(true, connection, fs);
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
