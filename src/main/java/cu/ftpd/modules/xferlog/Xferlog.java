/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.xferlog;

import cu.ftpd.user.User;
import cu.ftpd.logging.Logging;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-22 : 19:28:47
 * @version $Id: Xferlog.java 280 2008-11-24 18:52:18Z jevring $
 */
public class Xferlog {
    private PrintWriter xferlog;
    private static final DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy", Locale.ENGLISH);

    public Xferlog(String xferlogfile) throws IOException {
        try {
            xferlog = new PrintWriter(new FileOutputStream(new File(xferlogfile), true));
        } catch (FileNotFoundException e) {
            throw new IOException("Could not open xferlog for writing", e);
        }
    }

    protected Xferlog() {
    }

    public void log(String s) {
        xferlog.println(s);
        xferlog.flush();
    }

    public void shutdown() {
        xferlog.close();
    }

    public void download(File file, User user, long transferTime, long bytesTransferred, String type, String remoteHostname) {
        xferlog(file, user, transferTime, bytesTransferred, type, remoteHostname, false);
    }

    public void upload(File file, User user, long transferTime, long bytesTransferred, String type, String remoteHostname) {
        xferlog(file, user, transferTime, bytesTransferred, type, remoteHostname, true);
    }

    private void xferlog(File file, User user, long transferTime, long bytesTransferred, String type, String remoteHostname, boolean in) {
        // NOTE: Permissions.shouldLog(..) should be checked before any methods on this class is called.
        //http://www.wu-ftpd.org/man/xferlog.html
        try {
            String filename = file.getCanonicalPath();
            if (filename.contains(" ")) {
                filename = "\"" + filename + "\"";
            }
            String line =   time.format(new Date()) + ' ' +
                            transferTime + ' ' +
                            remoteHostname + ' ' +
                            bytesTransferred + ' ' +
                            filename + // spaces are taken care of on next line
                            ("ASCII".equalsIgnoreCase(type) ? " a _ " : " b _ " ) +
		                    // todo: support "d" for "deleted" as well
                            (in ? "i r " : "o r ") + // spaces are taken care of on the previous line
                            user.getUsername() + ' ' +
                            user.getPrimaryGroup() + // this is supposed to be service, but since this is a one-service thing, we might as well add some extra info here.
                            " 0 * " +
                            (bytesTransferred == file.length() ? "c " : "i ");

            // note: this'll have to do for now. Maybe later we'll upgrade to 'wu-ext' format
            log(line);
        } catch (IOException e) {
            Logging.getErrorLog().reportError("Failure while writing xferlog for: " + file.getAbsolutePath() + ". Cause: " + e.getMessage());
        }
    }
}
