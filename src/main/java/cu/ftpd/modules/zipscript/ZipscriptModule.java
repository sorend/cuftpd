/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.zipscript;

import cu.ftpd.modules.Module;
import cu.ftpd.modules.zipscript.shell.ShellZipscript;
import cu.ftpd.modules.zipscript.internal.CuftpdZipscript;
import cu.ftpd.modules.zipscript.actions.Rescan;
import cu.ftpd.modules.zipscript.external.PzsNgZipscript;
import cu.ftpd.modules.zipscript.eventhandler.ZipscriptEventHandler;
import cu.ftpd.events.EventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.ftpd.ServiceManager;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

import java.io.File;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-okt-02 - 12:05:18
 * @version $Id: ZipscriptModule.java 292 2009-03-04 19:44:36Z jevring $
 */
public class ZipscriptModule implements Module {
    private Zipscript zipscript;

    public void initialize(XMLSettings settings) throws ConfigurationException {
        switch(settings.getInt("/zipscript/type")) {
            case 4: // custom java class
                String classname = settings.get("/zipscript/custom/classname");
                if (classname == null || "".equals(classname)) {
                    throw new ConfigurationException("Please specify a classname for this tupe of zipscript");
                }
                try {
                    Class c = ServiceManager.getServices().getCustomClassLoader().loadClass(classname);
                    if (Zipscript.class.isAssignableFrom(c)) {
                        zipscript = (Zipscript)c.newInstance();
                    } else {
                        throw new ConfigurationException("Class " + c + " does not implement interface " + Zipscript.class.toString());
                    }
                } catch (ClassNotFoundException e) {
                    throw new ConfigurationException("Could not find custom zipscript class", e);
                } catch (IllegalAccessException e) {
                    throw new ConfigurationException("Could not create custom zipscript class", e);
                } catch (InstantiationException e) {
                    throw new ConfigurationException("Could not create custom zipscript class", e);
                }
                break;
            case 3: // shell
                zipscript = new ShellZipscript(settings.get("/zipscript/shell/rescan"), settings.get("/zipscript/shell/upload"), settings.get("/zipscript/shell/delete"));

            case 2: // pzs-ng
                String zipscriptPath = settings.get("/zipscript/pzs_ng/path");
                File f = new File(zipscriptPath);
                if (!f.exists() || !f.isDirectory()) {
                    throw new ConfigurationException("Must specify an existing directory for /zipscript/pzs_ng/path");
                }
                zipscript = new PzsNgZipscript(zipscriptPath);
                break;
            case 1: // internal
                zipscript = new CuftpdZipscript(settings.get("/zipscript/cuftpd/files"), new File(settings.get("/zipscript/cuftpd/template")), settings.get("/zipscript/cuftpd/short_name"));
                break;
            case 0: // NoZipscript
            default:
                zipscript = new NoZipscript();
                break;
        }
    }

    public void registerActions(SiteCommandHandler siteCommandHandler) {
        siteCommandHandler.registerAction("rescan", new Rescan(zipscript));
    }

    public void registerEventHandlers(EventHandler eventHandler) {
        eventHandler.addAfterEventHandler(Event.DELETE_FILE, new ZipscriptEventHandler(zipscript));
    }

    public void stop() {
    }

    public Zipscript getZipscript() {
        return zipscript;
    }
}
