/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript.internal;

import cu.ftpd.logging.Logging;

import java.io.*;
import java.util.HashMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-17 : 03:26:09
 * @version $Id: SfvFile.java 258 2008-10-26 12:47:23Z jevring $
 */
public class SfvFile implements Serializable {
    private final HashMap<String, Long> files;
    private final File sfvfile;

    // let this represent an actual file, with a list of Files and longs that correspond tot he content of the file?
    public SfvFile(File file) {
        files = new HashMap<String, Long>();
        sfvfile = file;
    }

    public void populate() {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(sfvfile)));
            String line;
            while((line = in.readLine()) != null) {
                if (!"".equals(line) && line.charAt(0) != ';') {
                    String[] t = line.split("\\s+");
                    if (t.length == 2) {
                        files.put(t[0], Long.valueOf(t[1],16)); // stole the radix from drftpd crew, not known why it is needed, but is...
                    } // otherwise we got some cruft in the sfv-file
                }
            }
        } catch (IOException e) {
            // it is bad if we can't read the .sfv if we believe we have it
            Logging.getErrorLog().reportCritical("Unable to read sfv file that was supposed to be there: " + sfvfile.getAbsolutePath());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Logging.getErrorLog().reportException("Failed to close input stream", e);
                    //e.printStackTrace();
                }
            }
        }
    }


    public HashMap<String, Long> getFiles() {
        return files;
    }


    public File getSfvfile() {
        return sfvfile;
    }

    public Long getChecksum(String filename) {
        return files.get(filename);
    }
}
