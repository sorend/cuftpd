package cu.ftpd.events;

import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.shell.ProcessResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetAddress;

/**
 * Avoid all the boilerplate when creating events. Makes for more readable code where the events are actually needed.
 *
 * @author markus@jevring.net
 */
public class EventFactory {
    public static Event deleteFile(User user, FileSystem fs, String path) throws FileNotFoundException {
        Event event = new Event(Event.DELETE_FILE, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        event.setProperty("file.path.real", fs.resolveRealPath(path));
        event.setProperty("file.path.ftp", fs.resolveFtpPath(path));
        event.setProperty("file.size", String.valueOf(fs.length(path)));
        event.setProperty("site.section", fs.getSection(path).getName());
        return event;
    }
    
    public static Event createDirectory(User user, FileSystem fs, String path) {
        Event event = new Event(Event.CREATE_DIRECTORY, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        event.setProperty("file.path.real", fs.resolveRealPath(path));
        event.setProperty("file.path.ftp", fs.resolveFtpPath(path));
        return event;
    }

    public static Event removeDirectory(User user, FileSystem fs, String path) {
        Event event = new Event(Event.REMOVE_DIRECTORY, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        event.setProperty("file.path.real", fs.resolveRealPath(path));
        event.setProperty("file.path.ftp", fs.resolveFtpPath(path));
        return event;
    }

    public static Event renameFrom(User user, FileSystem fs, String path) throws FileNotFoundException {
        Event event = new Event(Event.RENAME_FROM, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        String realPath = fs.resolveRealPath(path);
        event.setProperty("rename.source.path.real", realPath);
        event.setProperty("rename.source.path.ftp", fs.resolveFtpPath(path));
        if (fs.isDirectory(realPath)) {
            event.setProperty("file.isDirectory", "true");
        } else {
            event.setProperty("file.isDirectory", "false");
            event.setProperty("file.size", String.valueOf(fs.length(path)));
        }
        return event;
    }

    public static Event renameTo(User user, FileSystem fs, String path, Event renameFromEvent) throws FileNotFoundException {
        //Event event = new RenameToEvent(user, fs.getRealPwd(), fs.resolveFile(target), fs.getRenameSource());
        //Event event = new RenameToEvent(user, fs.resolveRealPath(fs.pwd()), fs.pwd(), fs.resolveRealPath(target), fs.getRenameSource());
        Event event = new Event(Event.RENAME_TO, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        event.setProperty("rename.source.path.real", fs.getRealRenameSource());
        event.setProperty("rename.source.path.ftp", fs.getFtpRenameSource());
        event.setProperty("rename.target.path.real", fs.resolveRealPath(path));
        event.setProperty("rename.target.path.ftp", fs.resolveFtpPath(path));

	    boolean isDirectory = Boolean.parseBoolean(renameFromEvent.getProperty("file.isDirectory"));
	    if (isDirectory) {
		    event.setProperty("file.isDirectory", "true");
	    } else {
		    event.setProperty("file.isDirectory", "false");
		    event.setProperty("file.size", renameFromEvent.getProperty("file.size")); // This works since we haven't executed the rename yet
	    }
	    
	    /* because glftpd can't handle when we call commands between RNFR and RNTO, we have to grab this information from the RNFR
        if (fs.isDirectory(fs.getRealRenameSource())) {
            event.setProperty("file.isDirectory", "true");
        } else {
            event.setProperty("file.isDirectory", "false");
            event.setProperty("file.size", String.valueOf(fs.length(fs.getFtpRenameSource()))); // This works since we haven't executed the rename yet
        }
        */
        return event;
    }

    public static Event siteCommand(User user, FileSystem fs, String command) {
        //Event event = new SiteCommandEvent(user, command, fs.getRealPwd());
        //Event event = new SiteCommandEvent(user, fs.resolveRealPath(fs.getFtpParentWorkingDirectory()), fs.getFtpParentWorkingDirectory(), command);
        Event event = new Event(Event.SITE_COMMAND, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        event.setProperty("site.command", command);
        return event;
    }

    public static Event upload(User user, FileSystem fs, File file, InetAddress remoteHost, long bytesTransferred, long transferTime, ProcessResult pcr, String state) {
        return transfer(Event.UPLOAD, user, fs, file, remoteHost, bytesTransferred, transferTime, pcr, state);
    }

    public static Event download(User user, FileSystem fs, File file, InetAddress remoteHost, long bytesTransferred, long transferTime, String state) {
        return transfer(Event.DOWNLOAD, user, fs, file, remoteHost, bytesTransferred, transferTime, null, state);
    }
    
    private static Event transfer(int type, User user, FileSystem fs, File file, InetAddress remoteHost, long bytesTransferred, long transferTime, ProcessResult pcr, String state) {
        Event event = new Event(type, user, fs.getRealParentWorkingDirectoryPath(), fs.getFtpParentWorkingDirectory());
        event.setProperty("transfer.state", state);
        event.setProperty("transfer.bytesTransferred", String.valueOf(bytesTransferred));
        event.setProperty("transfer.timeTaken", String.valueOf(transferTime));
        if (pcr != null) {
            event.setProperty("transfer.postprocessor.exitvalue", String.valueOf(pcr.exitvalue));
            event.setProperty("transfer.postprocessor.message", pcr.message);
        }
        event.setProperty("file.path.real", file.getAbsolutePath());
        event.setProperty("file.path.ftp", FileSystem.resolvePath(file));
        event.setProperty("filesystem.transfer.type", fs.getType());
        event.setProperty("connection.remote.host", (remoteHost == null ? "n/a - connection failure" : remoteHost.getHostAddress()));
        return event;
    }
}
