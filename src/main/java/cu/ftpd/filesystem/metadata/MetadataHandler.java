/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.filesystem.metadata;

import cu.ftpd.persistence.Persistent;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-12 : 21:27:12
 * @version $Id: MetadataHandler.java 258 2008-10-26 12:47:23Z jevring $
 */
public class MetadataHandler implements Persistent {
    // File.getAbsolutePath(), Directory
    private final Map<String, Directory> directories = Collections.synchronizedMap(new HashMap<String, Directory>());

    // NOTE: this turned out to be much simpler than previously anticipated.

    public void setOwnership(File file, String username, String group) {
        setOwnership(file, username, group, false);
    }

    public void setOwnership(File file, String username, String groupname, boolean recursive) {
        synchronized(directories) {
            if (recursive) {
                if (file.isDirectory()) {
                    for (File f : file.listFiles(cu.ftpd.filesystem.FileSystem.forbiddenFiles)) {
                        setOwnership(f, username, groupname, recursive);
                    }
                }
                // we always do this, since we need to change the directory itself, if it is a directory
                setOwnership(file, username, groupname, false);
            } else {
                getDirectory(file.getParentFile()).setOwnership(file, username, groupname);
            }
        }
    }

    public void delete(File file) {
        Directory d = getDirectory(file.getParentFile());
        if (d != null) {
            d.delete(file);
        } else {
            //System.err.println("Could not find parent for deleted file. It must have been removed before we got a change to alter the metadata in our system. It doesn't matter though, since it is deleted now anyway.");
        }
    }

    public void move(File source, File target, String username, String group) {
        synchronized(directories) {
            /*
            different if we are moving files or directories.
            have checks so that we can't move a directory to a file (or vice versa, as it is actually rename)
            This will have been taken care of by checking if the move was successful or not
             */

            Directory sourceParent = getDirectory(source.getParentFile());
            Metadata sourceOwnership = sourceParent.delete(source);
            Directory targetParent = getDirectory(target.getParentFile());
            if (username == null && group == null) {
                if (sourceOwnership != null) {
                    targetParent.setOwnership(target, sourceOwnership.getUsername(), sourceOwnership.getGroupname());
                } // if it was null, then nobody owned it, and thus nobody should own it after the move
            } else {
                targetParent.setOwnership(target, username, group);
            }
            if (target.isDirectory()) {
                Directory targetDirectory = getDirectory(target);
                targetDirectory.setFile(target);
            }

        }
    }

    public Directory getDirectory(File directory) {
        synchronized(directories) {
            if (directory.isFile()) {
                throw new IllegalStateException(directory.getAbsolutePath() + " was supposed to be a directory, not a file! How the hell did you end up here?!");
            }
            /*
            System.out.println("looking for " + directory.getAbsolutePath() + " in directories: ");
            for (String s : directories.keySet()) {
                System.out.println(s);
            }
            */
            Directory d = directories.get(directory.getAbsolutePath());
            if (d == null) {
                // it plain just didn't exist, create it and return it.
                //System.out.println("creating Directory for " + directory.getAbsolutePath());
                d = new Directory(directory);
                d.load();
                directories.put(directory.getAbsolutePath(), d);
            }

            return d;
        }
    }

    public void enterDirectory(File directory) {
        Directory d = getDirectory(directory);
        d.userEnter();
        //System.err.println("USER ENTER: " + directory.getAbsolutePath());
    }

    public void exitDirectory(File directory) {
        Directory d = getDirectory(directory);
        d.userExit();
    }

    public void save() {
        final long now = System.currentTimeMillis();
        synchronized(directories) {
            Iterator<Directory> i = directories.values().iterator();
            while (i.hasNext()) {
                Directory d = i.next();
                if ((now - d.getLastUpdate()) > 15000 && d.visitors() == 0) {
                    // throw it out if it is older than 15 seconds and didn't have anybody holding a reference to it.
                    i.remove();
                    //System.err.println("EXPUNGING old directory from cache: " + d.getFile().getAbsolutePath());
                }
            }
        }
    }

    public boolean hasOwner(File file) {
        return getMetadata(file) != null;
    }

    public boolean isOwner(String username, File file) {
        Metadata metadata = getMetadata(file);
        return metadata != null && metadata.getUsername().equals(username);
    }

    private Metadata getMetadata(File file) {
        File parent = file.getParentFile();
        if (parent != null) {
            Directory d = getDirectory(parent); // d can never be null, so we're cool
            return d.getMetadata(file.getName());
        } else {
            // if parent == null, that means that we are checking of the root ("/", "c:\", "d:\", etc) has a parent, which it doesn't, and as a result we return null.
            return null;
        }
    }

    /**
     * Finds the owner of a specified file.
     * @param file the file for which we want to discover the owner.
     * @return the username of the owner. <code>null</code> if the file has no owner, or doesn't have any metadata.
     */
    public String getOwner(File file) {
        Metadata m = getMetadata(file);
        if (m != null) {
            return m.getUsername();
        } else {
            return null;
        }
    }

    /**
     * Finds the group of a specified file.
     * @param file the file for which we want to discover the group.
     * @return the name of the group. <code>null</code> if the file has no group, or doesn't have any metadata.
     */
    public String getGroup(File file) {
        Metadata m = getMetadata(file);
        if (m != null) {
            return m.getGroupname();
        } else {
            return null;
        }
    }
}
