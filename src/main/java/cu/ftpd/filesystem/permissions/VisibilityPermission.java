/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.filesystem.permissions;

import java.util.Set;
import java.util.HashSet;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-mar-10 - 20:09:08
 * @version $Id: VisibilityPermission.java 307 2011-02-27 19:10:52Z jevring $
 */
public class VisibilityPermission extends Permission {
    protected final int visibility;
    protected final Set<String> observerGroups = new HashSet<String>();
    protected final Set<String> observerUsers = new HashSet<String>();
    protected final Set<String> subjectGroups = new HashSet<String>();
    protected final Set<String> subjectUsers = new HashSet<String>();
    protected final Set<String> subjectFiles = new HashSet<String>();
    protected final Set<String> subjectDirectories = new HashSet<String>();

    public VisibilityPermission(int visibility, boolean quick, String path) {
        super(quick, path);
        this.visibility = visibility;
    }

    public void addObserverGroup(String group) {
        observerGroups.add(group);
    }

    public void addObserverUser(String username) {
        observerUsers.add(username);
    }

    public int getVisibility() {
        return visibility;
    }

    public boolean appliesToObserverUser(String username) {
        return observerUsers.contains("*") || observerUsers.contains(username);
    }

    public boolean appliesToObserverGroups(Set<String> groups) {
        if (observerGroups.contains("*")) {
            return true;
        } else {
            for (String group : groups) {
                if (observerGroups.contains(group)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addSubjectGroup(String group) {
        subjectGroups.add(group);
    }

    public void addSubjectUser(String username) {
        subjectUsers.add(username);
    }

    public boolean appliesToSubjectUser(String username) {
        return subjectUsers.contains("*") || subjectUsers.contains(username);
    }

    public boolean appliesToSubjectGroups(Set<String> groups) {
        if (subjectGroups.contains("*")) {
            return true;
        } else {
            for (String group : groups) {
                if (subjectGroups.contains(group)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addSubjectDirectory(String directory) {
        subjectDirectories.add(directory);
    }

    public void addSubjectFile(String filename) {
        subjectFiles.add(filename);
    }

    public boolean appliesToSubjectFile(String filename) {
        return subjectFiles.contains("*") || subjectFiles.contains(filename);
    }

    public boolean appliesToSubjectDirectory(String directory) {
        return subjectDirectories.contains("*") || subjectDirectories.contains(directory);
    }

    public String toString() {
        return '[' + (visibility == HIDE ? "hide" : "show") +
                super.toString() + // this will get path and quick
                ";og:" + observerGroups +
                ";ou:" + observerUsers +
                ";sg:" + subjectGroups +
                ";su:" + subjectUsers +
                ";sf:" + subjectFiles +
                ";sd:" + subjectDirectories +
                ']';
    }
}
