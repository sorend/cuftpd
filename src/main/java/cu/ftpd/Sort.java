package cu.ftpd;

import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.userbases.local.LeechEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class Sort {
	private static final Comparator<User> usersByCaseInsensitiveUsername = new Comparator<User>() {
		@Override
		public int compare(User o1, User o2) {
			return String.CASE_INSENSITIVE_ORDER.compare(o1.getUsername(), o2.getUsername());
		}
	};

	private static final Comparator<Connection> connectionsByCaseInsensitiveUsername = new Comparator<Connection>() {
		@Override
		public int compare(Connection o1, Connection o2) {
			String u1 = "<unknown>";
			if (o1.getUser() != null) {
				u1 = o1.getUser().getUsername();
			}
			String u2 = "<unknown>";
			if (o2.getUser() != null) {
				u2 = o2.getUser().getUsername();
			}
			return String.CASE_INSENSITIVE_ORDER.compare(u1, u2);
		}
	};

	private static final Comparator<Group> groupsByCaseInsensitiveName = new Comparator<Group>() {
		@Override
		public int compare(Group o1, Group o2) {
			return String.CASE_INSENSITIVE_ORDER.compare(o1.getName(), o2.getName());
		}
	};

	private static final Comparator<LeechEntry> leechEntriesByCaseInsensitiveUsername = new Comparator<LeechEntry>() {
		@Override
		public int compare(LeechEntry o1, LeechEntry o2) {
			return String.CASE_INSENSITIVE_ORDER.compare(o1.getUsername(), o2.getUsername());
		}
	};
	
	
	// unfortunately we can't call them all byUsername(), as erasure means that they have the same signature

	/**
	 * Sorts a collection of users by case insensitive username.
	 * 
	 * @param users the users to be sorted
	 * @return a sorted list of users
	 */
	public static List<User> users(Collection<? extends User> users) {
		List<User> sorted = new ArrayList<>(users);
		Collections.sort(sorted, usersByCaseInsensitiveUsername);
		return sorted;
	}

	/**
	 * Sorts a collection of connections by case insensitive username.
	 *
	 * @param connections the connections to be sorted
	 * @return a sorted list of connections
	 */
	public static List<Connection> connections(Collection<Connection> connections) {
		List<Connection> sorted = new ArrayList<>(connections);
		Collections.sort(sorted, connectionsByCaseInsensitiveUsername);
		return sorted;
	}

	/**
	 * Sorts a collection of groups by case insensitive name.
	 *
	 * @param groups the groups to be sorted
	 * @return a sorted list of groups
	 */
	public static List<Group> groups(Collection<? extends Group> groups) {
		List<Group> sorted = new ArrayList<>(groups);
		Collections.sort(sorted, groupsByCaseInsensitiveName);
		return sorted;
	}

	/**
	 * Sorts a collection of leech entries by case insensitive username.
	 *
	 * @param leechers the leech entries to be sorted
	 * @return a sorted list of users
	 */
	public static List<LeechEntry> leechers(List<LeechEntry> leechers) {
		List<LeechEntry> sorted = new ArrayList<>(leechers);
		Collections.sort(sorted, leechEntriesByCaseInsensitiveUsername);
		return sorted;
	}
}
